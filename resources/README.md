# Resources

[back](/)

A compiled list of interesting resources found from wandering about the internet.

All is here because of curiosity and learning purposes. I am not to be hold responsible and may not agree with every opinion these resources present. Nevertheless I gather them here because I belive there are knowledge that can be attained by consuming them.

I haven't read all of theese resources; those I haven't yet read through are marked with a '\*' ( anasterisk);

## Index

* [Computer and programming](#computer-and-programming)
	* [Language-specific](#language-specific)
		* [C/C++](#cc)
		* [Erlang](#erlang)
	* [Learn](#learn-programming)
		* [Programming](#programming)
		* [InfoSec](#infosec)
	* [Security and secure programming](#security-and-secure-programming)

## Computer and programming

### Language-specific

#### C/C++

* [C++ Core Guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines): ISO C++'s coding Guideline
* [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html): Google's C++ coding guideline

#### Erlang

* [\*Erlang Coding Standards & Guidelines](https://github.com/inaka/erlang_guidelines): Inaka's Erlang programming guideline
* [\*Programming Rules and Conventions](http://www.erlang.se/doc/programming_rules.shtml): Erlang's programming rules

### Learn

#### Programming

* [CodeWars](https://www.codewars.com/): Programming chalenges and rank-based (kyu) progession with community contribution
* [SoloLearn](https://www.sololearn.com/): Free courses and interactive activities

#### InfoSec

* [/r/hacking](https://www.reddit.com/r/hacking/comments/a3oicn/how_to_start_hacking_the_ultimate_two_path_guide/): Probably one of the best places to start
* [CTF101](https://ctf101.org/): Great way to learn about hacking and a few of the categories infosec has
* [HackThisSite](https://www.hackthissite.org/): Learn by doing it! Mainly web-exploiting
* [Microcorruption](https://microcorruption.com/login): Learn by doing it! Low-level with binary exploits and reverse engineering
* [OverTheWire](https://overthewire.org/wargames/): Learn by doing it! Linux-based
* [PicoCTF](https://picoctf.com/): Learn by doing it! All types of hacking and great difficulty scaling

### Security and secure programming

* [Common Vulnerabilities and Exposures](https://cve.mitre.org/): A list of all known vulnerabilities wuth an unique identifier to each.
* [\*Secure Programs HOWTO](https://dwheeler.com/secure-programs/Secure-Programs-HOWTO.html): Book about best secure programming practices and common vulnerabilities.

