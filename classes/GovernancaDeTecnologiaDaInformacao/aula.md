# Gestão de Tecnologia da Informação

## Introdução

Eu sei os seguintes tópicos agora no começo da matéria:

* PMBOK
* PRINCE 2
* Segurança
* Desenv. Aplicações
* SCRUM

## Governança

A área de governança

* Gerencia
* Delega
* Várias coisas

É importante definir regras de **como**, **quando**, **quem**, **qual**.

## Pilares de Sistemas de Informação

* Gestão de Serviços
* Desenvolvimento de Aplicações
* Segurança
* Gestão de Processos
* Gestão de Projetos
* Gestão de Qualidade
* ?

A ideia é que, no final da matéria você saiba qual desses pilares você se encaixa ou quer ir para.

Com isso é possível se planejar e estudar. Focar na área.

