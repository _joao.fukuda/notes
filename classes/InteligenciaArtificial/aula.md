# Aula 01

Ementa foi reformulada para focar na aplicação da IA na tecnologia.

Vamos passar por 3 grandes áreas:

* Representação de conhecimento
* Aprendizado de máquina
* Tópicos avançados de IA

## Avaliação

* 1 trabalho
* 1 prova (não síncrona, 1 semana de duração)

## Trabalho prático

* max 5 pessoas

Implementação de uma rede MultiLayer Perceptron, usando BackPropagation simples.

### 2 Vídeos

#### Vídeo 1

Teste e resultado de treinamento com imagens de caracteres.

#### Vídeo 2

Explicação da estrutura do código.

## Prova

* P fazer em casa

# Aula 2

## Algoritmo

**Conjunto de regras** que define uma **sequencia** (bem definidas) de operações com **dados** que **termina**.

Algoritmos não precisam de computador.

# Máquina de Turing

* Uma cabeça
* Uma fita com vários elementos
* Máquina de estado
* Pode remover, sobrescrever ou ler esses elementos

Todos os computadores, por enquanto, são uma máquina de Turing.

Computador é discreto. Qualquer ilusão de continuidade é somente uma aproximação.

## Inteligencia artificial

* Melhorar elas mesmas
* Usar linguagem
* Formar abstrações e conceitos

### IA Forte

Consciência do raciocínio

### IA Fraca

Simula raciocínio

## Testes de Turing

### Jogo de imitação

Ser humano $A$ conversa com $B$ e $C$, $B$ e $C$ são um ser humano e uma máquina (indiferente).

A ideia é que o computador quer se passar por ser humano. Se conseguir, é sucesso.

### Sala Chinesa

Recebe um dado, consulta a resposta ideal e entrega para a saída.

## Teste de consciência(?)

**EX MACHINA**

Teste de empatia. Se um humano sentir empatia por um computador, ele se torna consciente.

## Raciocínio

### Dedutivo

* Todo homem é mortal
* Sócrates é homem
* Logo, Sócrates é mortal

### Indutivo

* O **ferro** conduz calor
* O **ouro** conduz calor
* A **prata** conduz calor
* O **chumbo** conduz calor
* Logo, **todo metal** conduz calor

## Aprendizado de máquina

Aprende se é capaz de melhorar seu desempenho em determinada tarefa sob alguma medida de avaliação a partir de experiências passadas.

* Tipos
: Rede neural artificial, Árvore de decisão
* Desempenho
: Quão bem ele vai realizar a tarefa. Deve errar menos conforme ganha mais experiência
* Tarefa
* Medida
: Taxa de acerto do modelo
* Experiências

### NRA

Simula um neurônio.

Neurônio $N$ tem vários outros como entrada

Tem a intensidade da sua saída relacionada à entradas dos outros assim:

$N\_in_j = b_j + \sum_ix_iw_{ij}$

$N_j = f(N\_in_j)$

## Paradigmas de Aprendizado

### Supervisionado

Recebe um vetor que descreve um ambiente.

* Professor
: Sabe a resposta, mas não consegue aplicar em vetor de ambiente desconhecido
* Sistema:
: Tenta fazer a tarefa em ambientes antes desconhecidos

Sistema testa e bate a resposta com a do professor.
Os erros são usados para arrumar o sistema.

Requer um **label**. As respostas certas que o professor precisa para treinar o sistema.

### Não Supervisionado

Quase igual ao supervisionado, mas não tem um professor nem **labels**.

Faz um modelo sem direção à uma resposta específica.

Voltado a descoberta.

### Por Reforço

Escolha do agente interfere nas escolhas futuras.

O agente transforma o ambiente conforme vai andando sendo treinado.

Busca por recompensas positivas e evita recompensas negativas.

Estratégia de **Tentativa e erro e generalização**.

**Se tem um restaurante que eu gosto, o quanto eu me arrisco a tentar um restaurante novo?**

## Espaço vetorial & atributos descritivos

Conjunto não vazio cujos elementos são vetores (um ambiente).

## Extração de características

Construção da descrição do ambiente.

Escolher quais medições serão colocadas no vetor que representa o ambiente.

## Estratégia de Resolução de Problemas

### Clássica

Pipeline, tratamento e filtros podem ser utilizados.

### End-to-End

Algoritmo é responsável por todo o pipeline.

## Aprendizado Profundo

Usar aprendizado de máquina para descobrir ambos o mapeamento da representação (entrada) para a saída **E** a representação em si.

Extraindo conceitos

## Lógica proposicional

Proposições que podem assumir verdadeiro ou falso

Informação gerada a partir de outras informações

Ex.:

* Base de conhecimento
: Se esta chovendo então está nublado ($P \rightarrow Q$)
* Observação
: Está chovendo
* Conclusão
: Está nublado

Com lógica a gente consegue ter consistência (não interfere com outra regra da base de conhecimento) e completude (todas as frases que podem dar certo são construíveis).

## Lógica de primeira ordem

Lógica baseada em entidades/objetos.

Aprendendo conceitos

# Aula 3

## Nomenclatura

#### Vetores

Onde será guardado a representação do ambiente

#### Atributos (Descritores)

* Numéricos (Largura, Altura, Peso)
* Categóricos (Cor, Estado Civil)

*RG não é descritivo.*

### Visualização

Pode ser escolhido só alguns dos atributos; algumas dimensões para representar meu vetor. Representação vetorial.

#### Separabilidade linear

Se os vetores são separáveis facilmente dada uma visualização; alguma combinação de representação vetorial.

### Similaridade

Medir quão igual um vetor é do outro e **como** medir.

#### Distância espacial

##### Distância Euclidiana

(@) $$\sqrt{\sum|x_i-y_i|^2}$$

##### Distância Manhattan

(@) $$\sum|x_i-y_i|$$

##### Similaridade dos cossenos

$\frac{\langle x,y\rangle}{\|x\|_2\cdot\|y\|_2} = cos(0)$ sendo $\langle\rangle$ o produto interno e $\|\|_2$ a norma euclidiana e $\cdot$ uma multiplicação.

Similaridade dos cossenos é usado bastante para ver similaridades de pessoas que tem uma mesma proporção entre atributos.

### Normalização

Distribuir os dados em uma escala mais adequada. Pré-processamento de dados.

Reescalar os valores do meu atributo para que caiam em um intervalo específico (Ex.: $[0,1]$, $[-1,1]$ ou em volta da distribuição padrão dos valores).

### Reescala

Passar, por exemplo, de Fahrenheits para Celsius.

### Entropia

Medição ou comparação de coisas para ver quão diferentes elas são.

Erros introduzidos pelo ambiente na hora de medir/registrar dos atributos.

Você pode medir a entropia de grupos para ver quão bem separados eles estão.

Quanto menos entropia em todos os grupos, mais bem separados eles vão estar.

#### Baixa entropia

Muita informação (água e óleo). Fácil de ver diferenças.

#### Alta entropia

Pouca informação (café com leite). Difícil e muito energeticamente caro separar todas as informações.

#### Discretização

Transformar coisas contínuas em discretas.

Método básico de discretização é baseado na entropia de um único atributo.

## K-Means (?)

# Aula 4

## K-Nearest-Neighbor ou KNN (não muito inteligente)

Compara o dado que é novo com as tuplas do conjunto de treinamento.

Busca pelos K dados de treinamento mais próximos do novo dado armazenado na estrutura de dados.

Desses K vizinhos mais próximos, qual a classe que mais aparece. Essa classe será a atribuída ao novo dado.

### Regressão

Retornar um valor real para uma dada tupla desconhecida(em vez de retornar uma categoria).

Atribuir a média dos K elementos mais próximos ao novo vetor.

## Modelo

Conjunto de leis/regras que são treinadas e dão uma resposta a alguma coisa (dentro do universo limitado de possíveis respostas para o dado problema).

O modelo pode ser:

### Dado para o algoritmo

E iterado por ele para atualizar e melhorar ele.

### Criado pelo algoritmo

A partir de observação de experiências.

## Treinamento

Treinar o modelo kk

# Redes Neurais

Baseada em como um cérebro funciona

## Neurônios biológicos

* Dendrite
: Entradas, várias delas
* Axon
: Saída nervosa que se bifurca para mandar sinal para vários outros neurônios
* Synaptic gap
: Espaço entre um **dendrito** e o **axônio** de outro **neurônio**
* Limiar de disparo
: Mínimo para que um pulso possa ser repassado de um neurônio pelo axônio

### Vários tipos de neurônios

* Perceptron
* Winner-Take-All

### Cálculo

```
[x1]--w1-    -v1--[z1]
         \  /
         [x2]--w2--[y]
         /  \
[x3]--w3-    -v2--[z2]
```

* x1, x2 e x3
: Neurons de entradas que vem do mundo real
* w1, w2 e w3
: Pesos sinápticos que ponderam as energias que chegam para y
* z1 e z2
: Saída do processamento neural

## Perceptron Símples

Nomenclatura da Fausett

```
 bias->[1]
         \
[x1]--w1- b
         \ \
[x2]--w2--[y]<-neurônio de saída
         /
[x3]--w3-
```

(@) $$y\_in_j = b_j + \sum_i(x_i + w_{ij})$$

(@) $$y_j = f(y\_in_j)$$

* $f(y\_in_j)$ é uma função de ativação
* $y_j$ é a saída do neurônio $j$

## Funções de ativação básicas

### Step (ou threshold)

$f(x)$ é $1$ se $x \geq 0$, ou $0$ se $x < 0$

### Step (ou threshold) com limiar

$f(x)$ é $1$ se $x \geq \theta$, ou $-1$ se $x < \theta$

### Outra aqui que é +- reta

### Sigmoid

(@) $$\phi(v) = \frac{1}{1 + exp(-\alpha v)}$$

Varia entre $[0,1]$

## Treinamento simples

* Iniciar os pesos e os bias com 0 ou aleatório
* Determinar taxa de aprendizado $\alpha$
* Enquanto a condição de parada é falsa:
	* Para cada par de treinamento s : t, faça:
		* Determine as ativações das unidades de entrada $x_i = s_i$
		* Compute a resposta dos neurônios de saída
		* Atualize os pesos e bias se ocorrer algum erro (se $saida \neq t$) para o par de treinamento atual
	* Teste a condição de parada: se nenhum peso mudou na época, pare; senão, continue

Se $y \neq t$

(@) $$w_i(new) = w_i(old) + \alpha t x_i$$
(@) $$b(new) = b(old) + \alpha t$$

Se não,

(@) $$w_i(new) = w_i(old)$$
(@) $$b(new) = b(old)$$

# Multi-layer perceptron

* Camada de entrada
* Camadas escondidas (com função de ativação derivável em todos os pontos)
* Camada de saída (também com função de ativação derivável em todos os pontos)

No treinamento só pode mexer nos pesos. Não pode mexer nas funções de ativação, nas entradas e nem na estrutura da rede.

## Backpropagation

Achar o caminho dos pesos que leve ao ponto na superfície com o menor erro

O treinamento envolve 3 estágios:

* A passagem dos dados do treinamento
* O cálculo e retropropagação do erro
* O ajuste de peso

Erro do neurônio j é:

(@) $$e_j(n) = d_j(n) - y_i(n)$$

(@) $$\frac{1}{2}e_j^2(n)$$

(@) $$\Epsilon(n) = \frac{1}{2}\sum_{j \in C}e_j^2(n)$$

(@) $$\Epsilon_{av} = \frac{1}{N}\sum_{n=1}^{N}\Epsilon(n)$$

### O algorítmo

Muita coisa, ver o pdf

N ENTENDI:

* Campo induzido
* delta

Seguir a Fausett, Heiken ou alguma coisa assim, sla

#### Fausett

Taxa de aprendizado pode ser variada.

0. Inicialização de variáveis
1. Loop de época (passos 2-9)
2. Loop por cada dado (passos 3-8)
3. Estágio de feedfoward
4. ^
5. ^
6. retropropagação
7. ^
8. atualização de pesos
9. Condição de parada

