---
title: Registros de PET
subtitle: Turma 2020.212
author: João Francisco Cocca Fukuda - 10843566
---

# Aula 1 (18/08/2020) - Intrigado

Nessa aula fiquei intrigado após saber da proposta de trabalho pois ela me lembra de um outro projeto pessoal que comecei por influência de uma ideia que foi compartilhada em um curso online.

Nesse curso a pessoa que estava ministrando falou sobre a importância do registro de conhecimento para o avanço do ser humano. A ideia que ele deu foi de registrar todo o seu conhecimento sobre um tema (no caso do curso: computação) de forma palatável para outros e disponibilizar em um local de acesso público para que outras pessoas possam se beneficiar dessa sua busca pelo conhecimento.

Esse modelo de registro foca em tentar fazer com que a busca pelo mesmo conhecimento seja cada vez mais fácil e acessível a todos, ela foca em ajudar o próximo. Já o registro proposto para esse trabalho é mais focado no estado psicológico pessoal; ele foca em melhorar o indivíduo.

Eu achei muito curioso e interessante a aplicação do registro para o benefício pessoal, pois já me era claro que o registro do conhecimento poderia ajudar os outros, mas não tinha pensado (até hoje) em registrar o auto conhecimento e o usar para evoluir a si mesmo.

Estou também muito empolgado para as próximas aulas!

# Aula 2 (25/08/2020) - Curiosidade

O que mais me chamou a atenção na aula de hoje foi a conversa sobre entropia e como pessoas passaram a aceitar e integrar em seu estudo a instabilidade dos dados nas diferentes áreas do conhecimento. Minha curiosidade é sobre qual a incerteza que outras áreas do conhecimento tem que lidar e quais as soluções propostas para cada uma delas. Quantas áreas do conhecimento ainda não estão evoluindo pois é necessário um conhecimento de outra área para que essa evolua? As limitações do ser humano são muitas.

Achei interessante a conversa sobre entropia e incerteza pois estamos estudando na matéria de Engenharia de Software a lidar com elas na hora de exercer a profissão. Vimos a história e várias tentativas diferentes de como lidar com elas na nossa área; como desenvolver uma aplicação. Percebi que, com o passar do tempo, os profissionais -- assim como nas áreas do conhecimento discutidas em aula como física e química -- passaram de ignorar todas as incertezas e tentar trabalhar na nossa área como sendo uma ciência exata a enxergar que essa imprevisibilidade era um problema não a ser ignorado, mas tratado com cuidado.

Para nós conseguirmos lidar com essas incertezas que não víamos quando tratávamos essa área como puramente exata precisamos pedir ajuda de outras áreas do conhecimento como a psicologia e a sociologia pois esse conhecimento já existia, era só aplicar em outro domínio.

Hoje em dia se sabe que o melhor não é o profissional que sabe com certeza prazos, duração e custo, mas aquele que sabe qual o escopo e quais as incertezas que tem que ser levadas em conta na hora de exercer a profissão; é saber que a incerteza existe e tentar lidar o melhor possível com ela.

Achei interessante também saber sobre essa curiosidade da evolução do conhecimento: como os homo habilis lidavam com o desconhecido aplicando seu conhecimento para tentar explicar tudo e como esse conhecimento existe até hoje (mesmo que um pouco alterado).

# Aula 3 (?) - União

Gostei de como cientistas de várias áreas se juntaram e conseguiram descobrir tanto sobre o nosso passado.

# Aula 4 (08/09/2020) - Empatia

Na aula de hoje, o que mais me interessou foi falar sobre como a mudança de hábitos dos nossos ancestrais e do meio em que viviam afetou a interação interpessoal e como essas mudanças são vistas até hoje com ainda mais intensidade, mas com não tanta frequência quanto esperado. Essa falta de empatia me incomoda.

Muitas das pessoas hoje em dia não demonstram tais emoções ou não demonstram esses sentimentos que nos possibilitou evoluir tanto. Talvez (e muito provável) que isso esteja relacionado à criação da pessoa e a ilusão de superioridade; passamos a ser criados pensando em lucro num mundo onde tomar ações que nos beneficiem mesmo que prejudique o outro se tornou normal enquanto muitas pessoas se sentem constrangidas ao ajudar alguém. Isso me faz pensar que sentimentos agora virou sinal de fraqueza (e não podia estar mais longe da verdade).

# Aula 5

Origem da crença

# Aula 6



# Aula 7

Achei **muito** interessante saber como foi originado o patriarcado; toda a
história e teoria por trás desse tipo de sociedade que vivemos hoje assim como
teorias sobre as sociedades em si e como um único conhecimento (ou uma categoria
inteira de sentimentos) movem e regem um grupo inteiro de seres pensantes.

A ideia de que existe um sentimento prevalente no centro ideológico de cada
sociedade me fez pensar sobre os vários tipos diferentes de sociedades que
podem existir; assim como a empatia para uma sociedade matrística e a
competitividade para uma sociedade patriarcal pode existir sociedades que se
revolvam em outros sentimentos.

# Aula 8



# Aula 9

# Aula 10 (20/10/2020) - Crescimento

Hoje falamos bastante sobre a fase de crescimento do jovem e foi muito bom olhar para trás e finalmente *começar* a compreender essa minha fase que foi tão estranha de vivenciar (e não entender).

Eu imagino que não tive vários dos problemas que muitas pessoas tiveram durante a adolescência, mas sinto que não foi muito construtivo para o meu crescimento moral.

Achei interessante essa ideia do Lawrence Kohlberg de separar em 6 etapas o crescimento moral, mas discordo que isso é somente um fator; um caminho que se é trilhado. Sinto que estou, em alguns aspectos, já na fase de pós-convencional e, em outros, ainda na fase convencional, logo eu imagino que esse crescimento se assemelhe mais a um gráfico de radar do que uma barra de progresso.

# Aula 11 (27/10/2020) - Esclarecimento

Senti que muitas coisas da infância (a minha em particular) foram esclarecidas com essa aula. Acho que agora vou perceber e tentar identificar muito mais as singularidades dos sentidos dos outros.

Começando por mim: Eu percebi que tenho uma hipersensitividade tátil. Desde pequeno eu não consigo usar colares e cachecóis; anéis e outros acessórios "extras" sem sentir muito desconforto. Também não consigo ser tocado sem sentir um **grande** desconforto; Sem massagem para mim, toques no geral e prefiro piscina a praia por causa da areia dentre outras coisas que me vieram à cabeça e só agora sei o porquê de eu agir dessa forma.

Sinto que consigo entender melhor as pessoas e suas peculiaridades agora que tenho uma percepção maior do que pode estar acontecendo.

Muito obrigado pelas suas aulas, Professora! Todas elas são ótimas e muito esclarecedoras. Pena que a aula só tem dois créditos.

# Aula 12

Reflexão sobre o filme do menino com problemas de capacidade mental e suas
dificuldades despite seu 

# Aula 13

Qual o meu papel no grupo.

Não achei essa aula tão interessante quanto as outras, mas não foi
inaproveitada.

# Aula 14

Educação aula 1

# Aula 15

Educação aula 2

