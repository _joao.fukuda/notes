---
title: Introdução ao esclarecimento do pensamento humano
subtitle: PET - Turma das 8:00
author:
	- João Francisco Cocca Fukuda - 10843566
---

> Queria dedicar este trabalho ao meu eu do futuro. Muito obrigado por estar
> vivo e que continue sendo sempre curioso e feliz em aprender.

# Introdução

Este trabalho é a culminação de todas as reflexões feitas durante seis meses
estudando sobre psicologia e antropologia e sua aplicação prática em nós mesmos.

Tivemos a todo momento um tempo para pensar em todas as teorias e conceitos
apresentados na aula e ver como que eles se manifestam nos dias de hoje; nos
outros e em nós mesmos. Registramos todas essas reflexões e mantivemos um
diário onde é possível ver a evolução dos conceitos, o que mais intrigou na
matéria e uma reflexão final sobre o que pode ser concluído do conhecimento da
matéria.

Peço perdão logo já, pois meus relatos não estão completos. Tive momentos de
complicações um tanto quanto pessoais durante a matéria (mas não por causa dela)
que me tiraram todas as vontades; mas não cabe a este documento. Farei o melhor
com o que tenho registrado e, o que não tenho, farei uso das minhas memórias de
todas as aulas da matéria que, mesmo não registrando, ainda estive presente e
refleti sobre.

# Registros

## Intrigado

Nessa aula fiquei intrigado após saber da proposta de trabalho pois ela me
lembra de um outro projeto pessoal que comecei por influência de uma ideia que
foi compartilhada em um curso online.

Nesse curso a pessoa que estava ministrando falou sobre a importância do
registro de conhecimento para o avanço do ser humano. A ideia que ele deu foi
de registrar todo o seu conhecimento sobre um tema (no caso do curso:
computação) de forma palatável para outros e disponibilizar em um local de
acesso público para que outras pessoas possam se beneficiar dessa sua busca
pelo conhecimento.

Esse modelo de registro foca em tentar fazer com que a busca pelo mesmo
conhecimento seja cada vez mais fácil e acessível a todos, ela foca em ajudar
o próximo. Já o registro proposto para esse trabalho é mais focado no estado
psicológico pessoal; ele foca em melhorar o indivíduo.

Eu achei muito curioso e interessante a aplicação do registro para o benefício
pessoal, pois já me era claro que o registro do conhecimento poderia ajudar os
outros, mas não tinha pensado (até hoje) em registrar o auto conhecimento e o
usar para evoluir a si mesmo.

Estou também muito empolgado para as próximas aulas!

## Curiosidade

O que mais me chamou a atenção na aula de hoje foi a conversa sobre entropia e
como pessoas passaram a aceitar e integrar em seu estudo a instabilidade dos
dados nas diferentes áreas do conhecimento. Minha curiosidade é sobre qual a
incerteza que outras áreas do conhecimento tem que lidar e quais as soluções
propostas para cada uma delas. Quantas áreas do conhecimento ainda não estão
evoluindo pois é necessário um conhecimento de outra área para que essa evolua?
As limitações do ser humano são muitas.

Achei interessante a conversa sobre entropia e incerteza pois estamos estudando
na matéria de Engenharia de Software a lidar com elas na hora de exercer a
profissão. Vimos a história e várias tentativas diferentes de como lidar com
elas na nossa área; como desenvolver uma aplicação. Percebi que, com o passar
do tempo, os profissionais -- assim como nas áreas do conhecimento discutidas
em aula como física e química -- passaram de ignorar todas as incertezas e
tentar trabalhar na nossa área como sendo uma ciência exata a enxergar que
essa imprevisibilidade era um problema não a ser ignorado, mas tratado com
cuidado.

Para nós conseguirmos lidar com essas incertezas que não víamos quando
tratávamos essa área como puramente exata precisamos pedir ajuda de outras
áreas do conhecimento como a psicologia e a sociologia pois esse conhecimento
já existia, era só aplicar em outro domínio.

Hoje em dia se sabe que o melhor não é o profissional que sabe com certeza
prazos, duração e custo, mas aquele que sabe qual o escopo e quais as
incertezas que tem que ser levadas em conta na hora de exercer a profissão; é
saber que a incerteza existe e tentar lidar o melhor possível com ela.

Achei interessante também saber sobre essa curiosidade da evolução do
conhecimento: como os homo habilis lidavam com o desconhecido aplicando seu
conhecimento para tentar explicar tudo e como esse conhecimento existe até
hoje (mesmo que um pouco alterado).

## União

Gostei de como cientistas de várias áreas se juntaram e conseguiram descobrir
tanto sobre o nosso passado.

## Empatia

Na aula de hoje, o que mais me interessou foi falar sobre como a mudança de
hábitos dos nossos ancestrais e do meio em que viviam afetou a interação
interpessoal e como essas mudanças são vistas até hoje com ainda mais
intensidade, mas com não tanta frequência quanto esperado. Essa falta de
empatia me incomoda.

Muitas das pessoas hoje em dia não demonstram tais emoções ou não demonstram
esses sentimentos que nos possibilitou evoluir tanto. Talvez (e muito provável)
que isso esteja relacionado à criação da pessoa e a ilusão de superioridade;
passamos a ser criados pensando em lucro num mundo onde tomar ações que nos
beneficiem mesmo que prejudique o outro se tornou normal enquanto muitas
pessoas se sentem constrangidas ao ajudar alguém. Isso me faz pensar que
sentimentos agora virou sinal de fraqueza (e não podia estar mais longe da
verdade).

## Cultura

Gostei de saber um pouco como foi originada a cultura e os costumes e como isso
passou a ser algo intrínseco à nossa convivência.

## Sociedade

Achei **muito** interessante saber como foi originado o patriarcado; toda a
história e teoria por trás desse tipo de sociedade que vivemos hoje assim como
teorias sobre as sociedades em si e como um único conhecimento (ou uma categoria
inteira de sentimentos) movem e regem um grupo inteiro de seres pensantes.

A ideia de que existe um sentimento prevalente no centro ideológico de cada
sociedade me fez pensar sobre os vários tipos diferentes de sociedades que
podem existir; assim como a empatia para uma sociedade matrística e a
competitividade para uma sociedade patriarcal pode existir sociedades que se
revolvam em outros sentimentos.

## Crescimento

Hoje falamos bastante sobre a fase de crescimento do jovem e foi muito bom
olhar para trás e finalmente *começar* a compreender essa minha fase que foi
tão estranha de vivenciar (e não entender).

Eu imagino que não tive vários dos problemas que muitas pessoas tiveram
durante a adolescência, mas sinto que não foi muito construtivo para o meu
crescimento moral.

Achei interessante essa ideia do Lawrence Kohlberg de separar em 6 etapas o
crescimento moral, mas discordo que isso é somente um fator; um caminho que se
é trilhado. Sinto que estou, em alguns aspectos, já na fase de pós-convencional
e, em outros, ainda na fase convencional, logo eu imagino que esse crescimento
se assemelhe mais a um gráfico de radar do que uma barra de progresso.

## Esclarecimento

Senti que muitas coisas da infância (a minha em particular) foram esclarecidas
com essa aula. Acho que agora vou perceber e tentar identificar muito mais as
singularidades dos sentidos dos outros.

Começando por mim: Eu percebi que tenho uma hipersensitividade tátil. Desde
pequeno eu não consigo usar colares e cachecóis; anéis e outros acessórios
"extras" sem sentir muito desconforto. Também não consigo ser tocado sem
sentir um **grande** desconforto; Sem massagem para mim, toques no geral e
prefiro piscina a praia por causa da areia dentre outras coisas que me vieram
à cabeça e só agora sei o porquê de eu agir dessa forma.

Sinto que consigo entender melhor as pessoas e suas peculiaridades agora que
tenho uma percepção maior do que pode estar acontecendo.

Muito obrigado pelas suas aulas, Professora! Todas elas são ótimas e muito
esclarecedoras. Pena que a aula só tem dois créditos.

# Meta reflexão

Vi que, em todas as aulas, a parte que mais achei intrigante se refere a algo
que pode ser aplicado ou a algum conhecimento de como surgiram as coisas em que
tanto nos apoiamos hoje.

Conhecimento que, uma vez obtido e compreendido (nem que a um nível
superficial), gere resultados perceptíveis a nível pessoal agregando ao modo de
ver o mundo; um conhecimento que nos deixa mais próximo à sociedade a
possibilitar a compreensão do funcionamento dessa.

Eu adoro ensinar e sinto que esse meu modo de ver e as coisas por que me
interesso tem bastante haver com isso. Minha empatia se manifesta no querer
ensinar aos outros ou, caso eu não consiga ajudar com o conhecimento, apoiá-los
na sua própria busca pelo conhecimento.

As coisas que eu aprendo são **todas** com outras pessoas em mente. Sempre estou
pensando em quem que adoraria ou se beneficiaria com o conhecimento e pensando
em novas maneiras de compartilhar esse conhecimento que eu tenho.

Conhecimento é algo incrível e **muito** poderoso; e a curiosidade gera
conhecimento. Eu sou movido pela curiosidade e adoro, por isso tento instigar a
curiosidade em todos. Sinto que o conhecimento está, hoje em dia, sendo muito
elitizado, por isso tento ser ao máximo informal e tentar instigar o máximo de
pessoas possível; seja na rua, na faculdade, em casa, com amigos, familiares, em
bares, restaurantes, ... Em todos os lugares e com qualquer um é um ótimo
momento de expressar sua curiosidade.

# Representação cultural da meta reflexão

Talvez eu não seja tão erudito a ponto de saber algum poema ou livro que
exprima essa conclusão tão bem. Mas há um filme que me vem à mente: "Sementes
podres".

É sobre um "professor" ensinando à uma classe de delinquentes. Esse professor
mesmo sabendo quase nada e não tendo intimidade com seus alunos ensina tudo o
que julga ser útil; tudo o que sabe. Sempre tendo eles em mente.

O que ele não sabe ou não tem o poder sobre, ele não abandona, mas guia esses
alunos apoiando-os a todo o momento. Sendo o porto seguro deles; alguém com quem
eles possam contar mesmo essa pessoa não sabendo pelo que estão passando, alguém
que te apoie mesmo não sabendo o por quê.

