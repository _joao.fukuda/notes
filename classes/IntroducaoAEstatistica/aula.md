# Aula 01

Estatística ta viva a muito tempo (triste).

Atualmente faz parte de todas as medidas econômicas e outras características de estudo da população.

A gente vai fazer voltada para a área de computação. A **ciência de dados**.

## Métodos descritivos

### Métodos gráficos

Insights valiosos e nada triviais.

## Métodos Tabulares

## Tipos de variáveis

* Discreta
* Continua
* Qualitativa
	* Nominal & Ordinal

## Nomenclatura

### População

Conjunto total de objetos sob análise.

### Amostra

Parte da população que será utilizada para análise.

Seleção de amostra é uma parte importante da inferência estatísticas.

### Experimento

Qualquer processo que gera resultados bem definidos. Ex.: cara ou coroa.

### Espaço amostral ($\Omega$ ou $S$)

Conjunto de todos os resultados possíveis do experimento. Ex.: para cara ou coroa, $S = {cara, coroa}$.

### Evento composto

Evento que consiste de mais do que um resultado. $E = {2, 4, 6}$ e $F = {1, 2, 3}$, $E \cup F = {1, 2, 3, 4, 6}$

### Complemento

Todos os eventos que não se encaixam no evento ($A'$).

### Disjuntos

Quando $A$ e $B$ não tem resultados em comum.

## Leis de Morgan

* $(\cup^{n}_{i=1}E_{i})^{c} = \cap^{n}_{i=1}E^{c}_{i}$
* $(\cap^{n}_{i=1}E_{i})^{c} = \cup^{n}_{i=1}E^{c}_{i}$

## Axiomas

* $P(A) \geq 0$ para qualquer evento $A$
* $P(\Omega) = 1$
* Se os eventos $A_{1}, A_{2}, ...$ são mutuamente exclusivos, então: $P(A_{1} \cup A_{2} \cup ...) = \sum^{\infty}_{i=1}P(A_{i})$

# Aula 02

## Probabilidade

$A \subset B, P(B) = P(A) + P(A^{c}B)$

$A \subset B, P(B) = P(A) + P(A^{c}B)$

$\binom{n}{r} = \binom{n-1}{r-1} + \binom{n-1}{r}$

## Probabilidade condicional

### Dois conjuntos mutuamente exclusivos

$P(A\cup B) = P(A) + P(B)$ e $P(A\cap B) = P(\emptyset)$

### Dois conjuntos exaustivos

$P(A\cup B) = P(\Omega)$

## Condicionamento

$P(B|A)$ significa qual a probabilidade de $B$ dado que $A$ aconteceu.

### Independência de eventos

$P(B|A) = P(B)$

### Dependência de eventos

$P(A|B) = \frac{P(A\cap B)}{P(B)} = \frac{P(AB)}{P(B)}$

$P(A\cap B) = P(AB) = P(A|B)\times P(B)$

# Aula 03

## Lançando 2 dados

$\Omega = {(1,1), (1,2), ..., (6,5), (6,6)}$

### Caso um dado seja 3

Atualizar o espaço amostral:

$\Omega = {(3,1), (3,2), (3,3), (3,4), (3,5), (3,6)}$

### Para que a soma de 8

$P(S8|P3) = \frac{1}{6}$

## Probabilidade total

Se $A_1, A_2, ..., A_k$ são mutuamente exclusivos e exaustivos.

### A probabilidade de qualquer evento $B$ é:

$P(B) = \sum^k_{i=1}P(B|A_i)\times P(A_i)$

### Com $P(A_i) > 0$, para $i = 1, 2, ..., k$. Então, para qualquer outro evento $B$:

$P(Aj|B) = \frac{P(A_j)\times P(B|A_j)}{\sum^k_{i=1}P(A_i)\times P(B|A_i)}$

## Eventos Independentes

Somente se para dois eventos $A$ e $B$, $P(A\cap B) = P(A)\times P(B)$

Para eventos $A_1, A_2, ..., A_k$, sendo eles mutuamente independentes,

$P(A_1 A_2 ... A_k) = \Pi^{k}_{i = 1}P(A_i)$

# Aula 4

## Variáveis aleatórias

Qualquer função que, dado um universo $\Omega$ de possibilidades, mapeia um valor real para cada um desses valores de $\Omega$.

### Tipos de dados

#### Qualitativos

* Nominais
* Ordinais

#### Quantitativos

* Discretos
* Contínuos

### Exemplo

Jogar duas moedas justas:

$H = Head = Cara$

$T = Tail = Coroa$

$\Omega = {(HH), (HT), (TH), (TT)}$

$x:\{$Numero de Caras$\}$

$x((HH)) = 2$

$x((HT)) = 1$

$x((TH)) = 1$

$x((TT)) = 0$

### Variáveis com características importantes

Geram distribuições de probabilidades conhecidas

#### V.a. de Bernoulli

Resulta em somente $0$ e $1$, ou Fracasso e Sucesso.

### Função Massa de probabilidade

Ou função de probabilidade de uma v.a discreta.

### Exemplo

Tem 20 bolas numeradas de 1 a 20, tirando 3 bolas de uma vez, qual a probabilidade de tirar uma bola maior ou igual a 17?

$\Omega = {1, 2, ..., 20}$

$x:\{$maior # selecionado$\}$

$P(x\geq 17) = (x = k)\frac{(^{k-1}_{2})}{(^{20}_{3})}$

### FDA (Função de Acumulada)

$P(x) = p(X\leq x)$

e

$p(a \leq X \leq b) = p(X \leq b) - p(a<X)$

ou, para inteiros

$p(a \leq X \leq b) = P(b) - P(a-1)$

### Valor Esperado

$E(X) = \mu_{X} = \sum_{x \in D}x \times p(X=x)$, onde $D$ são todos os valores possíveis de $X$ (a v.a).

### Variância

* $V(X)$
* $= \sigma^2$
* $= \sigma^2_X$
* $= \sum_{x \in D} p(X=x) \times (x - E(X))^2$
* $= [\sum_D x^2 \times p(x)] - \mu^2$
* $= E(X^2) - [E(X)]^2$

### Desvio padrão

$DP(X) = \sqrt{\sigma^2_X} = \sigma_X$

