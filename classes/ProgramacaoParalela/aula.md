# EPs

Vários EPs (3) e mini-EPs serão dados. Muito foco em programação.

* Com Make, CMake ou autotools
* Disciplina Avançada

## EPs

* 1
: threads, OpenMP
* 2
: CUDA
* 3
: 1 + 2 + Distribuido

# Participação

Ele quer muita participação

# Aulas

2 etapas

# Programação Paralela e Concorrente

Teoria dos jogos explica lojas e coisas que dependem da cooperação e boa vontade das pessoas.

# Programação Paralela e Distribuída

## EP

Fazer um programa...

## Bibliotecas que usam a GPU para multiprocessamento

* [OpenML](https://www.openml.org/)
* [OpenACC](https://www.openacc.org/)
* [OpenCL](https://opencl.org/)

## Bibliotecas para comunicação entre threads

* [OpenMPI](https://www.open-mpi.org/)

# ILP

Instruction Level Parallelism

## Pipeline

Dividir uma tarefa grande em diferentes "estágios", cada um tem seu ciclo paralelizado pela pipeline

* Fetch
: Pegar a instrução da memória
* Decode
: Transformar a instrução em algo físico para ser executado pela memória
* Prepare
: Preparar o estado da CPU para a operação
* Execute
: Executar a coisa na CPU
* Record
: Colocar os resultados de volta nos registradores

### Bolhas

Quando ocorre um cache miss, o cache é perdido e o pipeline fica ocioso por um momento.

## Spectre & Meltdown

### Meltdown

Ler em uma posição longe da memória onde você não tem acesso

* Fazer uma conta que traga algo para o cache
* Olhar o cache

```asm
mov al,memoria[val]
```

### Spectre

```c
if (x < sizeof array) {
}
```

## Tipos de paralelismo

### Iterativo

Processos que podem ter uma ou mais partes de um laço.

Comunicação depende da sincronização.

Trivialmente paralelizável.

### Recursivo

Concorrência usando chamadas recursivas. Ex.: merge sort; cálculo de integrais.

Divisão e conquista

### Produtor | Consumidor

Um processo produz, outro consome

Exige um buffer no qual o consumidor pega o que foi deixado pelo produtor

Pipe do unix é um bom exemplo

### Cliente | Servidor

Servidor aguarda e processa pedidos

Cliente solicita serviços e espera por respostas

### Interação entre pares

Vários processos que fazem a mesma coisa. Ex.: Programação paralela e distribuída

Descentralização de responsabilidade

Peer to peer

## Implementações de multithreading

### PThreads

Criar threads:
```c
pthread_t t;
pthread_create(&t, NULL, func, NULL);
pthread_exit(0);
```

`pthread_create(thread_to_put, options, function, (void(*)(void*))argument);`

### OpenMP

Prog simples:
```c
#pragma omp parallel
{
	puts("Olá, mundo em várias threads!\n");
}
```

Compartilhar e privar variáveis entre threads:
```c
int tid, i, nthreads;
#pragma omp parallel default(shared) private(i, tid)
{
	tid = omp_get_thread_num();
	printf("Thread #%d\n", tid);

	if (tid == 0) {
		nthreads = omp_get_num_threads();
		printf("# of threads: %d\n", nthreads);
	}
}
```

Definir threads para os programas:

* `OMP_NUM_THREADS=16 ./a.out`
* `omp_set_num_threads(16);`
* `#define OMP_NUM_THREADS 16`
* `gcc -DOMP_NUM_THREADS=16 main.c -fopenmp`

Paralelização de for:
```c
#pragma omp parallel for
for (i = 0; i != 10; ++i) {
	// Feito em paralelo
}
```

Separa de 4 em 4 iterações para cada thread:
```c
#pragma omp parallel for schedule(static, 4)
for (int i = 0; i != 10; ++i) {
	// Feito em paralelo
}
```

Possíveis típos de schedule:
* `static`
: specifically chunk-sized iterations, in order
* `dynamic`
: request n every time there is space
* `guided`
: like dynamic, but size of chunk is proportional to # of unassigned iterations / # of the threads
	chunk-size is minimum chunk size
* `auto`
: yep, auto
* `runtime`
: you define at runtime what it'll be (with `omp_set_schedule` or env `OMP_SCHEDULE`)

Soma todos os resultados (sem problema de sessão crítica):
```c
int result;

#pragma omp parallel for reduce(+:result)
for (int i = 0; i != 10; ++i) {
	result += i;
}
```

Reduce operations allowed: `+, -, *, &, |, ^, &&, ||`

Also allows:
* `++` and `--`
* `o=`

* [For & Schedule](http://jakascorner.com/blog/2016/06/omp-for-scheduling.html)
* [For & Reduce](http://jakascorner.com/blog/2016/06/omp-for-reduction.html)

