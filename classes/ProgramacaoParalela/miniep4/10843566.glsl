// Digite aqui seu código GLSL para gerar uma imagem
// O GLSL é muito próximo do C, mas não é idêntico.

// Essa linha vai definir a precisão do float,
// e mediump é bom o bastante no momento.
precision mediump float;

// Aqui não tem conversão automática entre int e float
// Então coloque .0 quando precisar de floats

// As janelas tem um tamanho fixo de 3000x3000
#define WIDTH (3000.0)
#define HEIGHT (3000.0)

// Constantes do limite do plano cartesiano
#define Xi (-2.0)
#define Xj (+1.0)
#define Yi (-1.5)
#define Yj (+1.5)

#define Xd (Xj - Xi)
#define Yd (Yj - Yi)

float interpolate(float A, float B, float percentage) {
	return sqrt(A*A*(1.0 - percentage) + B*B*percentage);
}

vec4 color_by_step (float step) {
	float red = 0.0;
	float green = 0.0;
	float blue = 0.0;

	step *= 255.0;
	if (step < 10.0) {
		step /= 10.0;
		red = interpolate(0.0, 32.0, step);
		green = interpolate(7.0, 107.0, step);
		blue = interpolate(100.0, 203.0, step);
	}
	else if (step < 80.0) {
		step = (step - 10.0)/70.0;
		red = interpolate(32.0, 237.0, step);
		green = interpolate(107.0, 255.0, step);
		blue = interpolate(203.0, 255.0, step);
	}
	else if (step < 150.0) {
		step = (step - 80.0)/70.0;
		red = interpolate(237.0, 255.0, step);
		green = interpolate(255.0, 170.0, step);
		blue = interpolate(255.0, 0.0, step);
	}
	else if (step < 200.0) {
		step = (step - 150.0)/50.0;
		red = interpolate(255.0, 100.0, step);
		green = interpolate(170.0, 120.0, step);
		blue = 0.0;
	}
	else {
		step = (step - 200.0)/55.0;
		red = interpolate(100.0, 0.0, step);
		green = interpolate(120.0, 0.0, step);
		blue = 0.0;
	}

	red /= 255.0;
	green /= 255.0;
	blue /= 255.0;

	return vec4(red, green, blue, 1.0);
}

// diferente do código em JS
// a GPU roda essa função main para cada pixel
void main() {
	// Aqui o pixel é guardado num vetor de 4 floats
	// Também segue o formato RGBA, mas dessa vez
	// os campos variam de 0.0 a 1.0

	// vamos obter as coordenadas do plano a partir da
	// variável gl_FragCoord que é pré definida para cada pixel
	vec2 xy = (gl_FragCoord.xy / vec2(WIDTH, HEIGHT) * vec2(Xd, Yd)) + vec2(Xi, Yi);

	// Para acessa o x ou o y individualmente é só usar
	// xy.x e xy.y respectivamente
	float x = xy.x;
	float y = xy.y;

	//****************************
	//compute a cor do pixel aqui

	// ex: pintar de acordo com a distância do centro
	// usaremos a função help para ajudar a computar a distância

	float zx = 0.0;
	float zy = 0.0;
	float step_normalized = 1.0;

	for (int step = 0; step < 255; ++step) {
		float new_zx = zx*zx - zy*zy + xy.x;
		float new_zy = 2.0*zx*zy + xy.y;
		zx = new_zx;
		zy = new_zy;
		if ((zx*zx + zy*zy) > 4.0) {
			step_normalized = float(step)/255.0;
			break;
		}
	}

	//*****************************
	// Aplica a cor
	gl_FragColor = color_by_step(step_normalized);
}

// Clique em Run GLSL para rodar o código ;)
// vim: filetype=c

