// Digite aqui seu código JS para gerar uma imagem

// As janelas tem um tamanho fixo de 3000x3000
const WIDTH = 3000;
const HEIGHT = 3000;

// Constantes do limite o plano cartesiano
const Xi = -2.0;
const Xj = +1.0;
const Yi = -1.5;
const Yj = +1.5;

const Xd = Xj - Xi;
const Yd = Yj - Yi;

const interpolate = (A, B, percentage) => {
	return Math.sqrt(A*A*(1 - percentage) + B*B*percentage);
};

const color_by_step = (step) => {
	let red = 0;
	let green = 0;
	let blue = 0;

	if (step < 10) {
		step /= 10;
		red = interpolate(0, 32, step);
		green = interpolate(7, 107, step);
		blue = interpolate(100, 203, step);
	}
	else if (step < 80) {
		step = (step - 10)/70;
		red = interpolate(32, 237, step);
		green = interpolate(107, 255, step);
		blue = interpolate(203, 255, step);
	}
	else if (step < 150) {
		step = (step - 80)/70;
		red = interpolate(237, 255, step);
		green = interpolate(255, 170, step);
		blue = interpolate(255, 0, step);
	}
	else if (step < 200) {
		step = (step - 150)/50;
		red = interpolate(255, 100, step);
		green = interpolate(170, 120, step);
		blue = 0;
	}
	else {
		step = (step - 200)/55;
		red = interpolate(100, 0, step);
		green = interpolate(120, 0, step);
		blue = 0;
	}

	return {red, green, blue};
}

const main = (data) => {
	//Data é um array de bytes que representa a imagem
	//O valor mínimo é 0 e o máximo é 255
	//A cada 4 bytes temos um pixel
	//Red Green Blue Alpha

	//Vamos usar o for para percorrer cada pixel
	for(let i = 0; i < data.length; i += 4) {
		const _x = Math.floor((i/4) % 3000);
		const _y = Math.floor((i/4) / 3000);

		//converte para um plano cartesiano indo de Xi-Xj e Yi-Yj.
		const x = Xi + (_x/WIDTH) * Xd;
		const y = Yj - (_y/HEIGHT) * Yd;

		//****************************
		//compute a cor do pixel aqui

		// ex: pintar de acordo com a distância do centro
		// (a distância máxima para os Xi-Xj e Yi-Yj atuais é raiz de 2)
		// (essa distância não é distância em si, eu sei)

		let zx = 0;
		let zy = 0;
		let step = 0;

		while (step < 255) {
			let zx_new = zx*zx - zy*zy + x;
			let zy_new = 2*zx*zy + y;
			zx = zx_new;
			zy = zy_new;
			if ((zx*zx + zy*zy) > 4) break;
			step += 1;
		}

		let {red, green, blue} = color_by_step(step);

		//*****************************

		//define a cor do pixel
		data[i]   = red;
		data[i+1] = green;
		data[i+2] = blue;
		data[i+3] = 255;
	}
};

// O nome da função a ser executada retornada deve aparecer no final
// clique em Run JS e aguarde um pouco para o seu código executar
main
