#!/bin/sh

compile_with_schedule_and_chunksize () {
   cp $SRC $SRC.bkp
   sed -Ei "s/(#pragma omp.*\$)/\1 schedule\($SCHEDULE, $((2**$N))\)/" $SRC
   make -B >/dev/null
   mv $SRC.bkp $SRC
}

compile_with_schedule() {
   cp $SRC $SRC.bkp
   sed -Ei "s/(#pragma omp.*\$)/\1 schedule\($SCHEDULE\)/" $SRC
   make -B >/dev/null
   mv $SRC.bkp $SRC
}

calculate_mean() {
   TIMES=()
   for _ in {1..10}
   do
      TIMES+=($(time -f %e $EXEC 2>&1 1>/dev/null))
   done
   echo -e Mean: "\x1b[1;94m"$(echo "(${TIMES[@]})/10" | sed 's/ /+/g' | bc -l)"\x1b[m"
}

main() {
   SCHEDULES=("static" "dynamic" "guided" "auto")
   EXEC=./parallelFor
   SRC=$EXEC.c

   make -B >/dev/null
   echo -e "With the \x1b[1;93mdefault\x1b[m schedule"
   echo -ne "\t"
   calculate_mean

   for SCHEDULE in ${SCHEDULES[@]}
   do
      echo -e "With schedule \x1b[1;93m$SCHEDULE\x1b[m"
      echo -e "\tDefault chunk-size"
      compile_with_schedule
      echo -ne "\t\t"
      calculate_mean
      if [ $SCHEDULE != "auto" ]
      then
         for N in {0..7}
         do
            echo -e "\tChunk-size = $((2**$N))"
            compile_with_schedule_and_chunksize
            echo -ne "\t\t"
            calculate_mean
         done
      fi
   done
}

main
