// miniEP1
// João Francisco Cocca Fukuda, 10843566
// g++ -o miniep_cpp miniep.cpp

#include <iostream>
#include <vector>

int main()
{
	int N;
	std::cin >> N;
	N = 1 << N;
	int primes = 0;
	int special_primes = 0;
	std::vector<bool> sieve(N+1);
	for (auto i = 2; i <= N; ++i) {
		sieve[i] = true;
	}
	for (auto i = 2; i <= N; ++i) {
		if (sieve[i]) {
			++primes;
			if (i%4 != 3) ++special_primes;
			for (auto j = i*2; j <= N; j += i) {
				sieve[j] = false;
			}
		}
	}
	std::cout << primes << ' ' << special_primes << std::endl;
}

