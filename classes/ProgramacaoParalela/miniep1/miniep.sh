#!/bin/bash
# miniEP1
# João Francisco Cocca Fukuda, 10843566
# bash miniep.sh

read INPUT
N=$((1 << $INPUT))
PRIMES=0
SPECIAL_PRIMES=0
for I in $(seq 0 $N)
do
	SIEVE[$I]=1
done
for I in $(seq 2 $N)
do
	if [ ${SIEVE[$I]} -eq 1 ]
	then
		PRIMES=$(($PRIMES + 1))
		if [ $(($I % 4)) -ne 3 ]
		then
			SPECIAL_PRIMES=$(($SPECIAL_PRIMES + 1))
		fi
		for J in $(seq $(($I * 2)) $I $N)
		do
			SIEVE[$J]=0
		done
	fi
done
echo $PRIMES $SPECIAL_PRIMES

