cin <- file('stdin', 'r')
input <- readLines(cin, 1)
close(cin)
as.numeric(input)
n <- bitwShiftL(1, input)
primes <- 0
specialprimes <- 0
sieve <- c()
for (i in seq(2, n-1)) {
	sieve[i] <- TRUE
}
for (i in seq(2, n-1)) {
	if (sieve[i]) {
		primes = primes + 1
		if ((i%%4) != 3) {
			specialprimes = specialprimes + 1
		}
		if (i*2 <= n) {
			for (j in seq(i*2, n, i)) {
				sieve[j] <- FALSE
			}
		}
	}
}
print(paste(primes, specialprimes))
