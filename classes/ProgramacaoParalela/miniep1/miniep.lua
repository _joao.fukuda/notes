n = 2^io.read("*n")
primes = 0
specialprimes = 0
sieve = {}
for i = 2,n do
	sieve[i] = true
end
for i = 2,n do
	if sieve[i] then
		primes = primes + 1
		if i%4 ~= 3 then
			specialprimes = specialprimes + 1
		end
		j = i*2
		while j <= n do
			sieve[j] = false
			j = j + i
		end
	end
end
io.write(primes.." "..specialprimes.."\n")
