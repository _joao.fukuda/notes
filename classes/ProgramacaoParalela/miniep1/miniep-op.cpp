// miniEP1
// João Francisco Cocca Fukuda, 10843566
// g++ -o miniep_cpp miniep.cpp

#include <iostream>
#include <boost/dynamic_bitset.hpp>

int main()
{
	long unsigned int N;
	std::cin >> N;
	N = 1 << N;
	long unsigned int primes = 0;
	long unsigned int special_primes = 0;
	boost::dynamic_bitset<> sieve;
	sieve.resize(N+1, true);
	for (boost::dynamic_bitset<>::size_type i = 2; i <= N; ++i) {
		[[unlikely]] if (sieve[i]) {
			++primes;
			[[likely]] if (i%4 != 3) ++special_primes;
			for (auto j = i*2; j <= N; j += i) {
				sieve[j] = 0;
			}
		}
	}
	std::cout << primes << ' ' << special_primes << std::endl;
}

