#!/bin/bash

N=24

for EXEC in miniep_*
do
	echo Timing $EXEC
	for _ in {1..5}
	do
		( /bin/time -f %e ./$EXEC ) 2>&1 > /dev/null <<< $N
	done
done

