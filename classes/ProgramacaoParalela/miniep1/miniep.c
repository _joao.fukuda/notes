// miniEP1
// João Francisco Cocca Fukuda, 10843566
// gcc -o miniep_c miniep.c

#include <stdio.h>
#include <malloc.h>

int main()
{
	int N;
	scanf("%d", &N);
	N = 1 << N;
	int primes = 0;
	int special_primes = 0;
	int* sieve = (int*)malloc(sizeof(int)*(N+1));
	for (int i = 2; i <= N; i++) {
		sieve[i] = 1;
	}
	for (int i = 2; i <= N; i++) {
		if (sieve[i] == 1) {
			primes++;
			if (i%4 != 3) special_primes++;
			for (int j = i*2; j <= N; j += i) {
				sieve[j] = 0;
			}
		}
	}
	printf("%d %d\n", primes, special_primes);
}

