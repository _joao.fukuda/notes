n = 2^parse(Int64, readline())
primes = 0
specialprimes = 0
sieve = Array{Bool, 1}(undef, n+1)
for i = 2:n
	sieve[i] = true
end
for i = 2:n
	if sieve[i]
		global primes += 1
		if i%4 != 3
			global specialprimes += 1
		end
		j = i*2
		while j <= n
			sieve[j] = false
			j += i
		end
	end
end
println("$primes $specialprimes")
