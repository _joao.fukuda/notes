// miniEP1
// João Francisco Cocca Fukuda, 10843566
// gcc -O5 -o miniep_c miniep.c

#include <stdio.h>
#include <malloc.h>
#include <memory.h>

int main()
{
	int N;
	scanf("%d", &N);
	N = 1 << N;
	int primes = 0;
	int special_primes = 0;
	int sievelen = (N + 4) >> 3;
	unsigned char* sieve = malloc(sievelen);
	memset(sieve, ~0, sievelen);
	for (int i = 2; i <= N; ++i) {
		if (sieve[i >> 3] & (1 << (i & 7))) {
			++primes;
			if ((i & 3) != 3) ++special_primes;
			for (int j = i << 1; j <= N; j += i) {
				sieve[j >> 3] &= ~(1 << (j & 7));
			}
		}
	}
	printf("%d %d\n", primes, special_primes);
}

