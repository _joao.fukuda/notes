-module(miniep).
-export([main/0]).

main() ->
	{ok, [Input]} = io:fread("", "~d"),
	N = 1 bsl Input,
	Sieve = create_sieve(N + 1),
	{Primes, SpecialPrimes} = main_loop(N, Sieve),
	io:fwrite("~b ~b~n", [Primes, SpecialPrimes]),
	ok.

main_loop(N, Sieve) ->
	main_loop(N, 2, 0, 0, Sieve).
main_loop(N, I, Primes, SpecialPrimes, _) when I == N ->
	{Primes, SpecialPrimes};
main_loop(N, I, Primes, SpecialPrimes, Sieve) ->
	case sieve_at(I, Sieve) of
		true ->
			case I rem 4 of
				3 ->
					main_loop(N, I + 1, Primes + 1, SpecialPrimes, update_sieve(I, Sieve));
				_ ->
					main_loop(N, I + 1, Primes + 1, SpecialPrimes + 1, update_sieve(I, Sieve))
			end;
		false ->
			main_loop(N, I + 1, Primes, SpecialPrimes, Sieve)
	end.

create_sieve(N) ->
	create_sieve(N, []).
create_sieve(N, S) when N == 0 ->
	S;
create_sieve(N, S) ->
	create_sieve(N - 1, [true | S]).

sieve_at(_, []) ->
	throw("Out of bounds~n");
sieve_at(N, [Head]) when N == 0 ->
	Head;
sieve_at(_, [_]) ->
	throw("Out of bounds ");
sieve_at(N, [Head | _]) when N == 0 ->
	Head;
sieve_at(N, [_ | Sieve]) ->
	sieve_at(N - 1, Sieve).

update_sieve(Prime, Sieve) ->
	update_sieve(Prime, Sieve, Prime).
update_sieve(_, [_], 0) ->
	[false];
update_sieve(_, [Current], _) ->
	[Current];
update_sieve(Prime, [_ | Tail], 0) ->
	[false | update_sieve(Prime, Tail, Prime - 1)];
update_sieve(Prime, [Current | Tail], I) ->
	[Current | update_sieve(Prime, Tail, I - 1)].

