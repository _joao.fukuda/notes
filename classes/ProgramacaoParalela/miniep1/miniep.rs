// miniEP1
// João Francisco Cocca Fukuda, 10843566
// rustc -o miniep_rs miniep.rs

use std::io;

fn main()
{
	let mut input = String::new();
	io::stdin().read_line(&mut input).unwrap();
	let n = 1 << input.trim().parse::<i32>().unwrap();
	let mut primes = 0;
	let mut special_primes = 0;
	let mut sieve = vec![true; n+1];
	for i in 2..=n {
		if sieve[i] {
			primes += 1;
			if i%4 != 3 {
				special_primes += 1;
			}
			for j in ((i*2)..=n).step_by(i) {
				sieve[j] = false;
			}
		}
	}
	println!("{} {}", primes, special_primes);
}

