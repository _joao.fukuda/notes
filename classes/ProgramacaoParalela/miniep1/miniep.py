n = 1 << (int)(input())
primes = 0
specialprimes = 0
sieve = [True] * (n + 1)
for i in range(2, n):
	if sieve[i]:
		primes += 1
		if (i%4 != 3):
			specialprimes += 1
		for j in range(i*2, n, i):
			sieve[j] = False
print("%d %d" % (primes, specialprimes))

