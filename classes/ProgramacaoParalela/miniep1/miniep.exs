defmodule Miniep do
	def create_sieve(n, sieve \\ [])
	def create_sieve(n, sieve) when n == 0 do
		sieve
	end
	def create_sieve(n, sieve) do
		create_sieve(n - 1, [true | sieve])
	end

	def update_sieve(prime, sieve) do
		update_sieve(prime - 1, prime, sieve)
	end
	def update_sieve(n, _, [current]) do
		if (n == 0) do
			[false]
		else
			[current]
		end
	end
	def update_sieve(n, prime, [current | tail]) do
		if (n == 0) do
			[false | update_sieve(prime - 1, prime, tail)]
		else
			[current | update_sieve(n - 1, prime, tail)]
		end
	end

	def main_loop(_, _, primes, specialprimes, []) do
		{primes, specialprimes}
	end
	def main_loop(i, n, primes, specialprimes, [curr | sieve]) do
		if (curr) do
			primes = primes + 1
			sieve = update_sieve(i ,sieve)
			specialprimes = if (rem(i, 4) != 3) do
				specialprimes + 1
			else
				specialprimes
			end
			main_loop(i + 1, n, primes, specialprimes, sieve)
		else
			main_loop(i + 1, n, primes, specialprimes, sieve)
		end
	end
end

use Bitwise

n = 1 <<< (IO.gets("") |> String.trim |> String.to_integer)
primes = 0
specialprimes = 0
sieve = Miniep.create_sieve(n)
{primes, specialprimes} = Miniep.main_loop(2, n, primes, specialprimes, sieve)
IO.puts "#{primes} #{specialprimes}"

