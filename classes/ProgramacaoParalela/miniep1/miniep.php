<?php ini_set("memory_limit", "7G"); ?>
<?php
// miniEP1
// João Francisco Cocca Fukuda, 10843566
// php miniep.php

$N = 1 << (int)readline("");
$primes = 0;
$special_primes = 0;
$sieve =  [];
for ($i = 2; $i <= $N; $i++) {
	$sieve[$i] = true;
}
for ($i = 2; $i <= $N; $i++) {
	if ($sieve[$i]) {
		$primes++;
		if ($i%4 != 3) $special_primes++;
		for ($j = $i*2; $j <= $N; $j += $i) {
			$sieve[$j] = false;
		}
	}
}
echo("$primes $special_primes");

?>

