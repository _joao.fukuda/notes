#!/bin/bash

RUNS_PER_TEST=200

MIN_DELAY=$(tail -n 1 min_delay.log)
DELAY=$MIN_DELAY
DELAY_INTERVAL=$DELAY
echo -en "\e[1;93mStarting DELAY --\e[m"
while true; do
   echo -e "\e[1;93m> $DELAY\e[m"
   BIN_NAME="sleepSort_$DELAY"
   gcc -DDELAY=$DELAY -o $BIN_NAME sleepSort.c
   OUTPUT="$(./verifier.sh ./$BIN_NAME $RUNS_PER_TEST)"
   rm $BIN_NAME

   [ $VERBOSE ] && echo -e "Output: \e[4m$OUTPUT\e[m" >&2
   [ $VERBOSE ] && echo -en "DELAY jump: \e[1;93m$DELAY --( \e[92m" >&2
   DELAY_INTERVAL=$(($DELAY_INTERVAL/2))
   if [ "$OUTPUT" == "Ok" ]; then
      if [ $DELAY -lt $MIN_DELAY ]; then
         MIN_DELAY=$DELAY
         [ $VERBOSE ] && echo -n "(newMD=$MIN_DELAY) " 2>&1
         echo $MIN_DELAY >> min_delay.log
      fi
      [ $VERBOSE ] && echo -n "-" >&2
      DELAY=$(($DELAY - $DELAY_INTERVAL))
   elif [ $(($DELAY + $DELAY_INTERVAL)) -lt $MIN_DELAY ]; then
      [ $VERBOSE ] && echo -n "+" >&2
      DELAY=$(($DELAY + $DELAY_INTERVAL))
   fi
   [ $VERBOSE ] && echo -en "$DELAY_INTERVAL \e[93m)--\e[m" >&2

   if [ $DELAY_INTERVAL -eq 0 ]; then
      DELAY=$MIN_DELAY
      DELAY_INTERVAL=$DELAY
   fi
done
