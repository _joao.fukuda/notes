#!/bin/bash

gen_vector() {
   seq 1 $1 | tr ' ' '\n' | shuf | paste -s -d ' '
}

if [ $# -ne 2 ]; then
   echo "Numero errado de argumentos"
   exit 1
fi

EXECUTABLE=$1
N_OF_TEST=$2
N_OF_FAILURES=0
MAX_FAILURES=$(($N_OF_TEST/10))

for _ in $(seq 1 $N_OF_TEST); do
   VECTOR="$(gen_vector 100)"
   OUTPUT="$($EXECUTABLE $VECTOR)"
   IS_SORTED=$(echo $OUTPUT | tr ' ' '\n' | head -n -1 | sort -nc 2> /dev/null ; echo $?)
   if [ $IS_SORTED == 0 ]; then
      [ $VERBOSE ] && echo -e "\e[1;92m+1 Success\e[m" >&2
   else
      [ $VERBOSE ] && echo -e "\e[1;91m+1 Failure\e[m" >&2
      N_OF_FAILURES=$(($N_OF_FAILURES + 1))
      if [ $MAX_FAILURES -lt $N_OF_FAILURES ]; then
         [ $VERBOSE ] && echo -e "\e[1;43;30m($(($N_OF_TEST - $N_OF_FAILURES))/$N_OF_TEST)\e[m" >&2
         echo "Not Ok"
         exit 1
      fi
   fi
done

[ $VERBOSE ] && echo -e "\e[1;43;30m($(($N_OF_TEST - $N_OF_FAILURES))/$N_OF_TEST)\e[m" >&2
echo "Ok"
