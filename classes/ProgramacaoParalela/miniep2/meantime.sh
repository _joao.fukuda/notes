#!/bin/bash

INPUT=26
[ -n "$3" ] && INPUT=$3

echo -e "Averaging \e[1;94m$2\e[m,\e[1;94m $1\e[m times with input \e[1;94m$INPUT\e[m"
echo -e "Program output: \e[93m$($2 <<< $INPUT)\e[m"

# Running tests and storing their time in a comma separated list
RESULTS=$( ( for I in $(seq 1 $1); do echo -e -n "\r\e[1;91m($I/$1)\e[m" 1>&2; \time -f %U $2 <<< $INPUT 2>&1 >/dev/null; done ) | paste -s -d ',' )
echo -e -n "\r\e[1;92m($1/$1)\e[m"

# Transforming values into a math equation
VAULES=$(echo "($(echo $RESULTS | sed 's/,/+/g')) / $1")
echo -e " Execution time average: \e[1;92m$( echo "$VAULES" | bc -l ) seconds\e[m"

