// miniEP2
// João Francisco Cocca Fukuda, 10843566
// gcc -Ofast -o miniep miniep-op.c

#include <stdio.h>
#include <malloc.h>
#include <memory.h>

#define BYTEPOSFROMI(x) 1 << (x & 7) // 2^(x%8)

int main()
{
	unsigned int N;
	scanf("%u", &N);
	N = 1 << (N - 1);
	unsigned int primes = 1;
	unsigned int special_primes = 1;
	unsigned int sievelen = (N + 7) >> 3;
	unsigned char* sieve = malloc(sievelen);
	memset(sieve, ~0, sievelen);
	for (unsigned int i = 1; i <= N; i++) {
		if (sieve[i >> 3] & (BYTEPOSFROMI(i))) {
			++primes;
			if (!(i & 1)) ++special_primes;
			for (unsigned int j = i*3 + 1; j <= N; j += (i << 1) + 1) {
				sieve[j >> 3] &= ~(BYTEPOSFROMI(j));
			}
		}
	}
	printf("%d %d\n", primes, special_primes);
}

