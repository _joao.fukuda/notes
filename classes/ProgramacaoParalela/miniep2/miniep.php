<?php ini_set("memory_limit", "7G"); ?>
<?php
// miniEP2
// João Francisco Cocca Fukuda, 10843566
// php miniep.php

$N = 1 << ((int)readline("") - 1);
$primes = 1;
$special_primes = 1;
$sieve = array_fill(0, ($N + 7) >> 3, ~0);
for ($i = 1; $i < $N; $i++) {
	if ($sieve[$i >> 3] & (1 << ($i & 7))) {
		$primes++;
		if (!($i & 1)) {
			$special_primes++;
		}
		for ($j = $i*3 + 1; $j < $N; $j += ($i << 1) + 1) {
			$sieve[$j >> 3] &= ~(1 << ($j & 7));
		}
	}
}
echo("$primes $special_primes");

?>

