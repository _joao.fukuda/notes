// miniEP1
// João Francisco Cocca Fukuda, 10843566
// gcc -O5 -o miniep_c miniep.c

#include <stdio.h>
#include <malloc.h>
#include <memory.h>

int main()
{
	int N;
	scanf("%d", &N);
	N = 1 << N;
	int primes = 1;
	int special_primes = 1;
	int sievelen = (N + 8) >> 3;
	unsigned char* sieve = malloc(sievelen);
	memset(sieve, ~0, sievelen);
	for (int i = 3; i <= N; i+=2) {
		if (sieve[i >> 4] & (1 << ((i & 15) >> 1))) {
			++primes;
			if ((i & 3) != 3) ++special_primes;
			for (int j = i; j <= N; j += i << 1)
				sieve[j >> 4] &= ~(1 << ((j & 15) >> 1));
		}
	}
	printf("%d %d\n", primes, special_primes);
}

