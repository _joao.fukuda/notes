---
title: Engenharia de Software
author: João Francisco Cocca Fukuda
---

# Planeje e Documente

Tratar engenharia de software como um projeto de engenharia (civil, por exemplo).

## Cascata (Waterfall)

O modelo cascata define fases para o desenvolvimento desde o processo de pensamento

1. Análise de requisitos e especificação
: Quais as funções que o código deve exercer e o que deve acontecer.
1. Projeto de arquitetura
: Quais os módulos, classes e API que o software deve ter.
1. Implementação e integração
: Escrever o código e juntar todos os pedaços.
1. Verificação
: QA.
1. Operação e manutenção
: Colocar para rodar e garantir que ele continue funcionando.

Você não começa uma fase *antes* de terminar a fase anterior porque é caro demais concertar os erros depois da operação.

Caso as fases sejam feitas perfeitamente, o software vai sair mais barato e melhor, mas precisa de uma documentação **muito** mais extensa.

### Problema

> Isso é exatamente o que eu pedi, mas não é o que eu quero...

Muito caro.

## Ciclo de vida Espiral (Spiral Lifecycle)

Tentar desenvolver protótipos durante o desenvolvimento para reduzir o problema do modelo **Cascata**.

### Problema

O ciclo de vida do processo de desenvolvimento ainda é muito engessado e com uma duração muito longa.

## Rational Unified Process (RUP)

4 Fases de desenvolvimento

1. Iniciação
: Definir os requerimentos do software
1. Elaboração
:
1. Construção
:
1. Transição
:

6 disciplinas que devem ser cobertas durante o desenvolvimento.

1. Modelagem de negócio
1. Requerimentos
1. Análise e design
1. Implementação
1. Teste
1. Implantação

# Manifesto Ágil (e Métodos Ágeis)

* Indivíduos e interações
: em vez de processos e ferramentas
* Software que funciona
: em vez de
* Colaboração com o cliente
* Resposta a mudança

Abraçar mudanças como um fato (pois **vai** acontecer).

Refinam um protótipo incompleto até virar o produto final.

Test Driven Development (TDD)

## Programação Extrema (XP)

* Iterações curtas
* Simplicidade
* Testar sempre
* Reveja o código continuamente em pares

