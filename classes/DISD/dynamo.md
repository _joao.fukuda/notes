---
title: Amazon Dynamo
subtitle: Trabalho semanal de DSID
author: João Francisco Cocca Fukuda - 10843566
---

# Sobre

O *Dynamo* é um modelo de banco de dados *NoSQL* que permite o armazenamento seguro em um ambiente distribuído.

Ele foi desenvolvido pela *Amazon* para servir como base para seus vários serviços, incluindo o *Amazon DynamoDB* e, o mais conhecido *Amazon Simple Storage Service* (ou *Amazon S3*); seu foco é na escalabilidade, resistência a falhas, particionamento, replicação, versionamento e segurança do serviço.

Esses requerimentos são a consolidação de várias preocupações com relação ao desempenho das ferramentas (que implementam o *Dynamo*) em um ambiente profissional vindo de anos no mercado de serviços web.

# Como funciona

A implementação do *Dynamo* foi feita com abstração e transparência em mente para facilitar sua aplicação e manter as dificuldades responsabilidade da estrutura. O banco de dados expõe somente duas funções; o principal (e único) meio de comunicação entre os serviços e a estrutura.

## A partição

A estrutura, para conformar o requerimento de ser particionada e escalável, implementa seu sistema de maneira distribuída. Seus dados são armazenados em diferentes máquinas de um mesmo *data center*.

Para definir o local de armazenamento do objeto, o *Dynamo* utiliza um sistema DHT.

## Get e Put

A interface com o banco de dados é feita através dos métodos `get(key)` e `put(key, context, object)`.

O método `get(key)` é o mais fácil de entender. Seu propósito é o de resgatar dados no banco de dados e retorná-lo. Para fazer isso, utiliza o mesmo 

O `put(key, context, object)` é o mais importante dos dois.

