---
title: Apache Thrift
subtitle: Trabalho semanal de DSID
author: João Francisco Cocca Fukuda - 10843566
language: pt-br
---

# Sobre

O Apache Thrift[^1] é um *framework* que permite a comunicação entre programas. Desenvolvida e distribuida gratuitamente e como sendo *open-source*, é atualmente armazenada em seu repositório do github [^2]. O projeto atrai também a atenção de gigantes como o Facebook, que tem sua própria versão também gratuita em seu repositório[^3].

Seu objetivo é conectar diferentes aplicativos ou módulos sem nenhuma complicação independente de linguagem de programação. O Apache Thrift funciona através de módulos feitos na ferramenta por contribuintes e, para cada linguagem, tenta ser o menos intrusívo possível. A ferramenta utiliza de guias de estilo de programação da linguagem implementada para não complicar ao máximo a complexidade e facilitar a compreenção.

# Funcionalidades

O Apache Thrift conta com o apoio da comunidade para fazer seu *framework* funcionar em mais de 20 linguagens de programação, incluindo C++, C#, Erlang, Haskel, Java, Perl, PHP, Python e Ruby, entre várias outras[^4].

Para utilizar o Apache Thrift, é necessário primeiro descrever todas as estruturas e funções que serão utilizadas pelos outros programas. Essas são construídas a partir de recursos disponibilizados pela própria ferramenta[^5]:

* Types
: variáveis básicas que foram escolhidas para ser a base de toda a comunicação. Deve ser implementada por todas as linguagens que o *framework* suporta. São essas: `bool`, `byte`, `i16`, `i32`, `i64`, `double` e `string`.

* Containers
: guardam os *types* em estruturas mais sofisticadas. São essas: `list`, `set` e `map`.

* Structs
: funcionam como classes no paradigma de orientação a objeto.

* Services
: classes abstratas que contèm uma descrição de como é a estrutura, mas não guardam informação.

# Funcionamento

Para fazer a conexão entre os aplicativos não é necessário utilizar nenhuma biblioteca, módulo, classe ou qualquer outro tipo de código extra. O Thrift gera o código necessário, implementando maneiras nativas à linguagem de fazer essa conexão. A ideia é utilizar a menor quantidade possível de dependências externas (possível em certas linguagens).

O Apache Thrift funciona como um gerador de código, atuando antes do deselvolvimento do programa. Ele lê as descrições dos "serviços", identificadas pelo desenvolvedor, e gera o código baseado nessas configurações. O deselvolvedor então fica com uma estrutura de código símples para encaixar seu programa.

O Thrift tem a capacidade de gerar clientes e servidores em diferentes linguagens com a estrutura identificada para formar juntos uma aplicação funcional. Os programas se comunicam através de protocolos RPC (Chamadas Remotas de Procedimento, ou *Remote Procedure Call* em inglês).

## O geramento

Quando o Apache Thrift roda, ele navega por todos os arquivos do seu código substituindo e adicionando código antes da execução (em linguagens interpretadas) ou compilação (em linguagens compiladas) com o objetivo de fazer um programa que possa se comunicar através de chamadas de métodos com outros programas na mesma rede (normalmente um outro programa que faz parte de uma aplicação única).

Esse código gerado é único para cada linguagem e define uma conexão. O código gerado é propositalmente transparente (não tem encapsulamento, todos os seus membros são acessados diretamente) e não é definido nenhum tipo de erro por variável não inicializada; todas são, por padrão, inicializadas com um valor padrão. Isso permite ao programador fazer mais ajustes até depois do pré-processamento e adicionar as mecanicas que achar necessário.

De acordo com o documento do *Facebook Thrift*:

> Essa escolha foi motivada pelo desejo de facilitar o desenvolvimento da aplicação. Nosso objetivo principal não é fazer os desenvolvedores aprender uma rica nova biblioteca para a sua linguagem de escolha, e sim gerar código que os permitam trabalhar com ferramentas que são mais familiares para cada linguagem.

## A estrutura gerada pelo programa

Para facilitar o processo de desenvolvimento, o código é dividido em diferentes camadas que cuidam dos vários processos da comunicação.

* Server
: Fica em uma thread esperando e aceitando pedidos de comunicação de outros programas. Quando recebe uma comunicação, cria uma thread para o ***processor*** tratar o requerimento.

* Processor
: Processa pedidos de chamada de método e fica encarregado de chamar o ***protocol*** para tratar os dados recebidos. Ele básicamente lê requerimentos e dados dos outros e escreve de volta os resultados.

* Protocol
: Desencripta qualquer possível encriptação, trata os dados que estão organizados em estruturas de arquivos (ex.: *json*, *xml*) e traduz os dados recebidos por RPC para uma estrutura que a linguagem do programa possa usar.

* Transport
: Abstrai o processo de transportar dados e chamadas pelo sistema através da rede. Sua interface é utilizada pelo programa para se comunicar com os outros programas.

O código gerado pelo *framework* cria uma arquitetura de cliente/servidor TCP utilizando os elementos listados acima. Todas as estruturas são transparentes; elas não tem métodos e variáveis privadas e todos os acessos são diretos com o intúito de deixar o código mais simples de ler e entender. Isso permite uma customização e otimização fácil do código por parte do desenvolvedor.

## A comunicação

A comunicação utiliza conexão TCP e conceitos de RPC para gerar uma interação mais natural sem perder o sentimento de estar tudo em um único programa (e não diferentes programas se comunicando). Esse modelo de API permite fazer chamadas diretas a métodos pela rede, proporcionando uma experiência mais rápida e precisa sem perder a acurácia de uma biblioteca nativa.

O modelo RPC implementado é perfeito, pois o propósito desse modelo de API é simular chamadas de funções. A mesma coisa que um programa normalmente faz para interagir com outro pedaço de código (estando ou não na mesma base de código).

A identificação das funções por parte da interface RPC é feita através de um mapa hash que contém um par com o nome da função como chave e o ponteiro para a função como valor evitando comparação entre strings. Isso deixa o acesso às funções pela interface com um tempo constante, diminuindo o tempo gasto com buscas lineares comparando strings.

O mapa chave/valor também permite depurar com facilidade o código pois todas as chamadas a métodos feitas por outros programas através do RPC tem obrigatóriamente o nome do método chamado.

O programa consegue rodar utilizando somente uma thread, mas não é possível servir a multiplos clientes ao mesmo tempo ou processar outra coisa enquanto o programa espera a resposta da chamada de método remota. O Apache Thrift também se beneficia com multithreads

<!-- Referencias -->

[^1]: Apache Thrift - Home (<https://thrift.apache.org/>)
[^2]: Apache Thrift (<https://github.com/apache/thrift>)
[^3]: Facebook Thrift(<https://github.com/facebook/fbthrift>)
[^4]: Apache Thrift Language Support (<https://thrift.apache.org/docs/Languages>)
[^5]: Thrift: Scalable Cross-Language Services Implementation (<https://thrift.apache.org/static/files/thrift-20070401.pdf>)

