---
title: Trabalho semanal
subtitle: Feito em R
author: João Francisco Cocca Fukuda - 10843566
---

# Dataset criado em R

Executei esse código abaixo para gerar os números:

```{.r}
dist <- list(0, 0, 0, 0, 0)
dist[1:5] <- rnorm(5, mean=167, sd=5)
```

Fiquei com os resultados na lista `dist`{.r}, no ambiente R e copiei esses valores na tabela abaixo e no google sheets ([link](https://docs.google.com/spreadsheets/d/1YSwgJg6N9Cf80t-M-7E19-N7T5wBE5aQNjwpOoWEHHQ/edit?usp=sharing)).

----------- -------- -------- -------- -------- --------
lista/valor $x_{1}$  $x_{2}$  $x_{3}$  $x_{4}$  $x_{5}$
----------- -------- -------- -------- -------- --------
$X_{1}$     166.6665 174.8735 173.5795 169.1572 162.8005

$X_{2}$     171.5509 159.3392 164.1428 172.0736 177.4057

$X_{3}$     172.0169 162.7884 172.2828 163.8313 168.2839

$X_{4}$     166.8881 175.5631 161.6576 165.5189 163.5318

$X_{5}$     163.4412 164.2285 172.3315 161.5465 180.0257
--------------------------------------------------------

# Contas

## Média

$\overline{x} = \frac{1}{n}\sum_{i = 1}^{n}x_{i}$

```{.r}
for(d in dist) {
	print(mean(dist[x]))
}
```

------------------ --------
$\overline{X}_{1}$ 169.4154

$\overline{X}_{2}$ 168.9024

$\overline{X}_{3}$ 167.8407

$\overline{X}_{4}$ 166.6319

$\overline{X}_{5}$ 168.3147
---------------------------

## Medianas e desvio padrão

$SD(X) = \sqrt{\frac{1}{n - 1}\sum_{i = 1}^{n}(x_{i} - \overline{x})^{2}}$

```{.r}
for(d in dist) {
	sort(d)
}

for(d in dist) {
	print(median(d))
	print(sd(d))
}
```

`sd(d)`{.r} utiliza a correção de Bessel[^1].

------- -------- -------------
        mediana  desvio padrão
------- -------- -------------
$X_{1}$ 169.1572 4.962625

$X_{2}$ 171.5509 7.132422

$X_{3}$ 168.2839 4.443263

$X_{4}$ 165.5189 5.371802

$X_{5}$ 164.2285 7.73852
-------------------------------

## Média das médias

```{.r}
for(n in 2:5) {
	list <- c()
	for(i in 1:n) {
		list[i] <- mean(dist[[i]])
	}
	print(mean(list))
}
```

----------- --------
$M_{12}$    169.1589

$M_{123}$   168.7195

$M_{1234}$  168.1976

$M_{12345}$ 168.221
--------------------

# Perguntas

### Por que as estatísticas (média, mediana, desvio padrão) das amostras de $X$ diferem das estatísticas de $X$?

Porque os valores utilizados são somente um subconjunto incompleto do conjunto de possibilidades de $X$, logo, não conseguem representar $X$.

### Por que o desvio padrão das amostras não é igual a $\frac{\sigma}{\sqrt{n}}$ (o desvio padrão da distribuição amostral da média de $X$)?

Porque, mesmo que os valores utilizados sejam um subconjunto de $X$, ainda são uma aproximação. Os valores das amostras são mais espalhados e sua variância é maior em relação à variância das médias em relação à sua média.

### Por que a variância e portanto o desvio padrão das amostras é calculado tomando-se $(n-1)$ e não $n$?

Porque corrige o viés na estimativa da variância que vem do cálculo da média da amostra; a variância sempre vai ser menor que a variância real, visto que a média tem um viés com essa particular coleção de dados.

O único momento em que a variância não for menor que a variância real é quando ela for igual. Esse $(n - 1)$ permite uma melhor aproximação da variância obtida com a variância real. O método se chama Correção de Bessel.

### Por que a média das médias amostrais não é igual à média de $X$? (já que a média da distribuição amostral das médias de $X$ é igual à média de $X$, isto é, $E(\overline{X}) = \mu$)?

Porque os cálculos da média estavam enviezados, visto que o subconjunto não repressenta $X$, logo, a média da média difícilmente será $E(\overline{X}) = \mu$ (o que foi o caso).

### Em que situação a média das médias amostrais poderia se aproximar ou igualar a média de $X$?

Quando o número de amostras ou a quantidade de números por amostra tender a infinito.

### O que se pode dizer da distribuição amostral das medianas de $X$ se as medianas das amostras de $X$ se aproximam da média de $X$?

Pode-se dizer que a variância da distribuição amostral das medianas de $X$ é baixa.

[^1]: sd (<https://www.rdocumentation.org/packages/stats/versions/3.6.2/topics/sd>)

