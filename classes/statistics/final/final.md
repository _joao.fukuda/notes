---
title: Os impactos que liberação ao acesso à praia na Baixada Santista tem nos índices do Covid-19
subtitle: Um estudo de caso e prospecção sobre a situação do coronavírus no litoral do estado de São Paulo
author: João Francisco Cocca Fukuda - 10843566
locale: São Paulo
date: 2020
lang: pt-BR
---

### Nota para mim mesmo:

A ideia é comparar com outros lugares que abriram as praias como a Austrália, Califórnia ou até mesmo o litoral norte do Brasil. Verificar as diferenças, fazer uma comparação e prospecção e tirar conclusões a partir dos resultados obtidos.

\newpage

# Resumo

A pandemia do Covid-19 têm afetado muito o estilo de vida de todos os brasileiros e, muitos não conseguiram se adaptar à quarentena. Comércios locais estavam indo à falência e a população não estava mais aguentando. Visto que os índices apontavam para um final dessa pandemia, vários municípios decidiram abrir, controladamente, seus comércios não essenciais e liberar algumas atividades de lazer antes proibidas. Os municípios do litoral de São Paulo abriram suas praias para surfistas que ha tempos não praticavam o esporte, mas a qual custo? Esse estudo visa medir os impactos que a liberação do acesso à praia no litoral do estado de São Paulo causam.

# Introdução

No dia 20 de Março, Santos decretou estado de calamidade [@prefsantos-calam], adotando várias medidas contra o vírus propostas dia 19 de Março [@prefsantos-medidas19], com medidas adicionais impostas dia 3 de Abril através de um decreto publicado no mesmo dia [@prefsantos-decreto3].

Dentre essas medidas, a interdição da orla da praia. Do portal da prefeitura de santos:

> Restrição total de acesso a faixa de areia, incluindo barracas, cadeiras, guarda-sol e ambulantes na praia.

As atividades normalmente praticadas nas praias da Baixada Santista (que engloba os municípios de Santos, São Vicente, Guarujá, Cubatão, Praia Grande, Bertioga, Peruíbe, Itanhaém e Mongaguá) foram interditadas devido à proibições decretadas em seus municípios e, durante dois meses e meio foi implantado e imposto um regime rígido de quarentena para o controle da pandemia.

Durante esse período, foram criados, retificados e reforçados vários métodos de prevenção ao vírus como a obrigatoriedade de máscaras caseiras em público [@prefsantos-mascaras], o fechamento obrigatório de estabelecimentos (permitindo somente a abertura de estabelecimentos visados essenciais) [@prefsantos-estabel] e o controle do trânsito intermunicipal por vias rodoviárias [@prefsantos-rodoproi] e hidroviárias [@prefsantos-hidroproi].

Desde o começo do mês de Junho, os municípios vêm liberando o acesso às praias [@diariolitoral-guaruabre]. Atualmente, três das nove cidades já tem seu acesso à praia liberados.

Vários outros locais também já tem seu acesso à praia liberados. Esse é o caso da Austrália[@theindependent-ausopens], alguns condados dos estados da Flórida [@theguardian-floridaopens] e da Califórnia [@weeknews-californiaopens], Estados Unidos, e o litoral norte de São Paulo [@r7-litnorspopens].

## Motivação

A Baixada Santista é somente um de vários centros metropolitanos litorâneos e, devido à sua individual atração turística, vêm sendo cada vez mais assolada pela pandemia. Especificamente as cidades de Santos e São Vicente são uma das populações com o maior número relativo de idosos do nosso país. Esses fazem parte da zona de risco do Covid-19; são mais suscetíveis à sua contração e sofrem mais com os sintomas.

E, antes de tudo, morei e vivi no Guarujá (uma das cidades que compõe a Baixada Santista) e ainda tenho muitos contatos por lá. Pude ver e ouvir relatos de parentes e conhecidos sobre a situação alarmante do vírus na região. Me assusta o desacato às regras impostas não só dos que vivem por lá, mas também dos turistas que vêm de outros municípios para "desfrutar das praias e do mar".

Meu desejo é o de informar e conscientizar sobre os prospectos da cidade caso as coisas continuem como estão.

## Objetivo

Têm-se como objetivo a prospecção dos dados sobre o coronavírus referentes à Região da Baixada Santista e sua comparação aos dados caso não houvesse a liberação das praias. A comparação tem cunho informativo e serve somente para alertar a todos dos perigos de uma política pública de liberação em meio à pandemia (especialmente em cidades turísticas).

# Desenvolvimento

![Gráfico de carros](./plot01.png){width=256}

## Estudos sobre prospecção

# Resultados

Gráficos que mostram mortes de velinhos e tals.

# Conclusão

É ruim abrir as praias. Olha aqui o porquê:

# Referências

