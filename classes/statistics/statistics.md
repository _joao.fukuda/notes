---
title: Statistics
subtitle: Prof. Regis Rossi 2020
---

\newcommand{\casebegin}{\begin{cases}}
\newcommand{\caseend}{\end{cases}}

# Sumário

* [Variaveis aleatórias discretas](#va-discretas)
	* [Distribuição de Bernoulli](#distribuição-de-bernoulli)
	* [Distribuição de Binomial](#distribuição-binomial)
	* [Distribuição de Poisson](#distribuição-de-poisson)
* [Variaveis aleatórias contínuas](#va-contínuas)
	* [Distribuição Contínua](#distribuição-contínua)
	* [Distribuição Normal (Gaussiana)](#distribuição-normal-gaussiana)

# VA Discretas

A variavel aleatória é o que mede o que estamos observando.

$P(X = x_{i}) = P(A_{i})$

Ex.: 3 cara-ou-coroa. Você quer ver qual a probabilidade de pelo menos uma delas ser cara. X é o número de vezes que caiu cara durante todo o experimento.

## Esperança matemática

$E(X) = \sum_{i = 1}^{n} x_{i} P(X = x_{i})$

O resultado médio "esperado" levando em consideração o universo de possibilidades e a probabilidade de cada uma delas.

#### Propriedades da VA

1. $E(f) = f$ if $X(k) = f, k = 1, 2, ..., n$
1. $E(kX) = k E(X)$ when $k$ is constant
1. $E(X \pm Y) = E(X) \pm E(Y)$
1. $E(\sum_{i = 1}^{n} X_{i}) = \sum_{i = 1}^{n} E(X_{i})$
1. $E(aX \pm b) = a E(X) \pm b$, when $a$ and $b$ are constants)

## Variância

Mede a escala da distância da esperança matemática até a média de variância (o quanto pode variar de um resultado ao outro).

Variância: $Var(X) = E(X^{2}) - [E(X)]^{2}$,

onde $E(X^{2}) = \sum_{i = 1}^{n} x_{i}^{2} P(x_{i})$

## Desvio padrão

A distância em sí sem o problema de ter uma potência de dois alí.

$\sigma_{X} = \sqrt[2]{Var(X)}$

## Função de distribuição acumulada

Qual a probabilidade de cair $x_{i}$ ou algum outro valor menor que ele.

$F(x) = P(X \leq x) = \sum_{x_{i} \leq x} P(x_{i})$

## Distribuição de Bernoulli

Um evento ($X$) que pode ser somente 1 ou 0, verdadeiro ou falso, acontece ou não.

$$
\casebegin
1$, sucesso$\\
0$, fracasso$
\caseend
$$
com

$P(X = 0) = p$ and $P(X = 1) = q$

Assim, a VA X com probabilidade de Bernoulli tem a função assim (dada por):

$P(X = k) = p^{k}q^{1 - k}$

$X$ \~ $Bern(p)$

### A esperança e variância respectivamente

$E(X) = p$ e $Var(X) = pq = p(1 - p)$

## Distribuição Binomial

Em $n$ eventos independentes temos $p = sucesso$ e $q = fracasso$ como resposta sendo $p + q = 1$. X é o numero de sucessos em n tentativas.

$P(X = k) = \binom{n}{k}p^{k}q^{n - k} = \binom{n}{k}p^{k}(1 - p)^{n - k}$

$\binom{n}{k} = \frac{n!}{k!(n-k)!}$

$X$ \~ $Bin(n, p)$

> $k$ successes occur with probability $p^{k}$ and $n - k$ failures occur with probability $(1 - p)^{n - k}$. However, the $k$ successes can occur anywhere among the $n$ trials, and there are $\binom{n}{k}$ different ways of distributing $k$ successes in a sequence of $n$ trials.

### Esperança e variança respectivamente:

$E(X) = np$ e $Var(X) = npq = np(1 - p)$

## Distribuição de Poisson

Sucessos em um determinado intervalo. X = # de sucessos no intervalo.

$P(X = k) = \frac{e^{-\lambda}\lambda^{k}}{k!}$

$e = 2.71828$

$X$ \~ $Poisson(\lambda)$

### Esperança e variança

$E(X) = \lambda$ e $Var(X) = \lambda$

# VA Contínuas

Não pode usar o mesmo formato $P(X = x_{i})$ porque não tem valores discretos $x_{i}$. Precisa ser a probabilidade em um intervalo, não um único valor.

$f(x)$ é a função probabilidade de X e é válida se:

1. $\int_{-\infty}^{\infty}f(x)dx = 1$
1. $f(x) \geq 0, \forall x \in R_{X}$, $R_{X}$ é a região onde $X$ ta definida
1. $P(a < X < b) = \int_{a}^{b}f(x)dx$, para qualquer $a < b$ em $R_{X}$

$\int_{a}^{b}f(x)dx = P(a < X < b)$

## Distribuição acumulada

Dada por:

$F(x) = P(X \leq x) = \int_{-\infty}^{x}f(x)dx, x \in R$

Logo, do teorema fundamental do cálculo:

$\frac{d}{dx}\int_{-\infty}^{u}f(u)du = f(x)$

então

$f(x) = \frac{dF(x)}{dx}$

Facilitando:

$F(x) = \int_{-\infty}^{x}f(x)dx$

e

$f(x) = F'(x)$

## Média

$E(X) = \int_{-\infty}^{\infty}xf(x)dx$

## Variância

$V(X) = \int_{-\infty}^{\infty}(x - E(X))^{2}f(x)dx = \int_{-\infty}^{\infty}x^{2}f(x)dx - (E(X))^{2}$

## Desvio padrão

$\sqrt{V(X)}$

## Distribuição Contínua

$X$ é contínua com distribuição uniforme.

### f.d.p

$f(x) = \frac{1}{b - a}$, $a \leq x \leq b$

### média

$E(X) = \int_{a}^{b} \frac{x}{b - a}dx = \frac{0,5x^{2}}{b - a}\mid_{a}^{b} = \frac{b + a}{2}$

### variância

$Var(X) = \int_{a}^{b} \frac{(x - \frac{(a + b)}{2})}{b - a}dx = \frac{(x - \frac{(a + b)}{2})^{3}}{3(b - a)}\mid_{a}^{b} = \frac{(b - a)^{2}}{12}$

### f.d.a

$F(x) = \int_{a}^{x} \frac{1}{b - a}du = \frac{x}{b - a} - \frac {a}{b - a}$

$$
\casebegin
0$, se $x < a\\
\frac{x - a}{b - a}$, se $a \leq x < b\\
1$, se $x \geq b
\caseend
$$

## Distribuição Normal (Gaussiana)

***A mais importante*** 

Quando mais chances no meio e uma distrib menor nas pontas (ex.: distribuição de peso em uma população)

Parametros:

$\mu$, $-\infty < \mu < \infty$

e

$\sigma^{2}$, $\sigma > 0$

$f(x) = \frac{1}{\sqrt{2\pi\sigma}}e^{\frac{-(x-\mu)^{2}}{2\sigma^{2}}}$, $-\infty < X < \infty$

$X$ \~ $N(\mu, \sigma^{2})$

### Média

$E(X) = \mu$

### Variância

$Var(X) = \sigma^{2}$

<!-- Contas

b = 30 -> 60%
v = 20 -> 40%

X = # de v depois de uma tentativa

E(X) = ? -> 0.4
Var(X) = ? -> (0.4) - (0.4 * 0.4) -> 0.4 - 0.16 -> 0.24
ou
Var(X) = pq = 0.4 * 0.6 = 0.24

-->

