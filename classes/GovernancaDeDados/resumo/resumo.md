---
title: Resumo
subtitle: Texto Peter Weill
author: João Francisco Cocca Fukuda - 10843566
---

# Por que governança de TI é importante?

Os resultados de um estudo feito com 256 empresas em 26 países diferentes indica que uma governança de TI bem implementada aumenta em $40\%$ os retornos de um mesmo investimento feitos ambos na área de TI de uma empresa.

Governança de TI busca cinco valores: (1) clarificar os papéis e tarefas que TI tem na empresa, (2) medir e gerenciar o custo-benefício da área, (3) projetar praticas de TI para se encaixar às necessidades da empresa, (4) atribuir responsabilidade pelas mudanças organizacionais necessárias para beneficiar melhor a área de TI e (5) aprender com toda a implementação e sempre estar reutilizando e compartilhando recursos de TI.

A área de governança de TI de uma empresa visa esses valores através da especificação de *frameworks* para direitos de decisão e responsabilidade, e também encorajando comportamentos desejáveis do uso de TI. Comportamentos esses que consistem com os valores, estratégias, normas e culturas da empresa.

Governança de TI (diferente de gerenciamento de TI) foca não em tomar as decisões, mas em estruturar a empresa de forma que as pessoas certas tenham o nível de autonomia certa para exercer melhor o seu papel.

# Arquétipos de governança

Governança atua em cinco domínios dentro de uma empresa:

* Princípios de TI
: como TI deve se encaixar com o resto do negócio
* Arquitetura de TI
: conjunto de regras e políticas que regem o uso de TI
* Estratégias de infraestrutura de TI
: estratégias para trabalhar com o orçamento alocado para a área (gasto com coisas técnicas e humanas)
* Necessidade de aplicação em negócios
: especificar a necessidade de negócio de aplicações de TI desenvolvidas internamente ou compradas
* Investimento e priorização de TI
: quanto e como deve ser gasto os investimentos na área

E utilizam de seis arquétipos diferentes (e mutualmente exclusivos) para alcançar esses objetivos. Os arquétipos definem um grupo de pessoas por hierarquia e, para esses grupos, são dados direito de decisão ou de opinião nas decisões que envolvem algum dos cinco problemas citados acima.

Esses direitos podem (e devem, para grandes empresas) ser divididos conforme necessário por unidade de negócio, região ou até grupo de unidades de negócio.

* Monarquia de Negócios
: grupo de executivos seniores tomam decisões de TI. Normalmente recebem opiniões de várias fontes específicas.
* Monarquia de TI
: profissionais de TI tomam decisões de TI. Normalmente checa para ver se as regras implementadas fazem sentido para o setor de TI e propõe mudanças caso necessário.
* Feudal
: cada chefe toma as decisões que mais faz sentido às necessidades locais.
* Federal
: Vários grupos governamentais que balanceados e geridos por um grupo de pessoas variadas (normalmente envolve até duas camadas de distância na hierarquia). Esse sistema é um dos mais difíceis de se tomar decisões por ser composto por muitas pessoas e afetar vários grupos.
* Duopolítica de TI
: decisões são tomadas por dois grupos: um composto por executivos de TI e o outro por algum grupo de negócios. Na duopolítica **sempre** há a representação da área de TI e da área de negócios.

* Anarquia
: Na anarquia, pequenos grupos ou indivíduos tomam suas próprias decisões baseadas somente nas suas atuais necessidades.

# Como as firmas governam

## Direito a opiniões

A maioria das empresas pesquisadas dão direito a opinião para um grupo que engloba várias pessoas enquanto o direito a decisão fica para somente um grupo seleto de pessoas.

Dos sistemas para o direito a opinião, o mais usado é o sistema federal (mais de $80\%$).

Também é notado que várias empresas também procuram por opiniões de fontes de fora da empresa como fornecedores, parceiros, clientes e consultores.

## Direito a decisões

Aqui é mais utilizado sistemas que envolvem (ou permite envolver) um pequeno número de indivíduos.

O direito a decisões é muito mais diverso que o direito a opiniões, por isso foi mais interessante dividi-lo entre todos os domínios e falar de cada um individualmente.

* Para princípios de TI
: o sistema mais utilizado é o de duopolítica devido à junção da necessidade de ter uma opinião mais forte por parte de TI e para que o grupo de negócios tenham certeza de que essas decisões se alinhem com os objetivos da empresa.
* Para arquitetura de TI
: o sistema mais utilizado é o de monarquia de TI por serem decisões muito técnicas, mas esperam ter uma opinião sobre os objetivos da empresa através de opiniões de sistemas federais e duopolíticos.
* Para estratégias de infraestrutura de TI
: usa o sistema de monarquia de TI assim como *Arquitetura de TI*.
* Para necessidades de aplicações de negócio
: o sistema federal é o mais utilizado pois afeta várias áreas, não só a de TI.
* Para investimento e priorização de TI
: é utilizado três arquétipos quase que igualmente: monarquia de negócios, federal e duopolítico. Poucas empresas (menos de $9\%$) colocam decisões de investimento nas mãos de grupos de TI.

# Padrões de governança

Mesmo havendo similaridades e padrões entre empresas, ainda há uma grande diferença de empresa para empresas no jeito com que elas organizam a governança. Foram identificados cinco fatores que afetam essa organização: (1) objetivos desejáveis estratégicos e de performance, (2) estruturas organizacionais diferentes, (3) experiência de governança, (4) tamanho e diversidade da empresa e (5) diferenças industriais e regionais.

# Projetando e Avaliando Governança de TI

Ao avaliar a governança de TI, é indicado reiterar esse processo quantas vezes necessário: listar quais os sistemas que serão utilizados para ambas as permissões de opinião e decisão para cada um dos 5 domínios, definir quais os grupos que vão compor esses sistemas e, por último, analisar a estrutura para verificar se ela bate com a cultura, estrutura, estratégia e objetivos da empresa sempre que possível tendo em mente alguns fatores muito importantes.

De acordo com a pesquisa, há um total de oito fatores críticos para uma governança de TI efetiva:

## Fatores críticos para o sucesso

1. Transparência
: dos mecanismos de TI mara todos os gerentes
1. Projetar ativamente
: em vez de defensivamente
1. Reprojetar raramente
: pois isso leva tempo e esforço
1. Educar sobre governança de TI
: e ajudar os gerentes a entender e usar mecanismos de governança de TI
1. Simplicidade
: e foco em somente alguns objetivos
1. Processo de tratamento de exceções
: para nunca ser pego de surpresa
1. Design de governança em vários níveis
: e não deixe espaço para imprevistos
1. Incentivos alinhados
: para não haver desalinhamento no sistema de incentivo/recompensa de governança de TI
