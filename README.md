# Notes

This is my knowledge base.

With this, I try to capture all I learn and consume; **where** do I learn and **what** I learn.

If there's anything I'm missing or have found any errors or missinformation, please do not hold back and create an issue or a merge request.

## Where

[Resources](/resources/) is where all sites, documents and other sources if informations I've stumbled upon are gathered.

## What

All things I'm interested in and have learned about I try to document on the [Knowledge](/knowledge/).

